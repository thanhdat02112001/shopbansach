<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = 'orders';

    protected $fillable = ['order_status','created_at'];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_detail', 'order_id', 'product_id' );
    }

    public function order_detail()
    {
        return $this->hasMany(OrderDetail::class, 'order_id');
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'order_transaction_id');
    }
    public function getPriceAttribute($value){
        return number_format($this->price,0,',');
    }
}
