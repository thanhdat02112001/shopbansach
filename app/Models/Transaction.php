<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $table = 'transactions';

    public function order()
    {
        return $this->hasOne(Order::class, 'order_transaction_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'transaction_user_id');
    }
}
