<?php

namespace App\Models;

use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    use HasFactory;

    use SoftDeletes;

    protected $table = 'products';
    
    protected $guarded = [];
    
    public function category()
    {
        return $this->belongsTo(Category::class, 'product_category_id');
    }

    public function author()
    {
        return $this->belongsTo(Author::class, 'product_author_id');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'product_supplier_id');
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order_detail' ,'product_id', 'order_id');
    }

    public function order_detail()
    {
        return $this->hasOne(OrderDetail::class, 'product_id' );
    }

    
}
