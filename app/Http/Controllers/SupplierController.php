<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SupplierController extends Controller
{
    public function list_supplier(Request $request)
    {
        $admin = Admin::where('id', Session::get('id'))->first();
        if (isset($_GET['supplier_search'])) {
            $supplier_search = $_GET['supplier_search'];
            $suppliers = Supplier::where('supplier_name', 'LIKE', "%{$supplier_search}%")->paginate(10);
            $suppliers->appends($request->all());
            return view('admin-pages-content.suppliers.list-supplier', compact('suppliers', 'admin'));
        } else {
            $suppliers = Supplier::where('deleted_at', NULL)->paginate(10);
            return view('admin-pages-content.suppliers.list-supplier', compact('suppliers', 'admin'));
        }
    }

    public function create_supplier(Request $request)
    {
        $request->validate([
            'supplier_name' => 'required',
            'supplier_phone' => 'required',
            'supplier_email' => 'required',
            'supplier_address' => 'required'
        ]);
        Supplier::create($request->input());
        return redirect('admin-page/suppliers');
    }

    public function show_supplier($id)
    {
        $admin = Admin::where('id', Session::get('id'))->first();
        $supplier = Supplier::findOrFail($id);
        return view('admin-pages-content.suppliers.show-supplier', compact('admin', 'supplier'));
    }

    public function edit_supplier($id)
    {
        $admin = Admin::where('id', Session::get('id'))->first();
        return view('admin-pages-content.suppliers.edit-supplier', ['supplier' => Supplier::findOrFail($id), 'admin' => $admin]);
    }

    public function update_supplier(Request $request, $id)
    {
        $request->validate([
            'supplier_name' => 'required',
            'supplier_phone' => 'required',
            'supplier_email' => 'required',
            'supplier_address' => 'required'
        ]);
        $supplier = Supplier::findOrFail($id);
        $formValue = $request->input();
        $supplier->update($formValue);
        return redirect('admin-page/suppliers');
    }

    public function delete_supplier($id)
    {
        $supplier = Supplier::findOrFail($id);
        $supplier->delete();
        return redirect('admin-page/suppliers');
    }
}
