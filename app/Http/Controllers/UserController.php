<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Contracts\Service\Attribute\Required;

class UserController extends Controller
{

    public function resigter(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'address' => 'required',
            'phone_number' => 'required|unique:users,phone_number',
        ]);

        $formValue = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'address' => $request->input('address'),
            'phone_number' => $request->input('phone_number')
        ];
        User::create($formValue);
        return redirect('/login');
    }
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $user = User::where('email', $request->email)->first();
        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            $request->session()->put('user', [
                'id' => $user->id,
                'username' => $user->name
            ]);

            return redirect("/");
        }
        $request->session()->flash('error', 'Đăng nhập thất bại');
        return redirect('/login');
    }
    public function logout()
    {
        Auth::logout();
        session()->forget('user');
        return redirect('/login');
    }
}
