<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Author;
use App\Models\Category;
use App\Models\Product;
use App\Traits;
use App\Models\Supplier;
use GuzzleHttp\RetryMiddleware;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Symfony\Component\Process\Process;


class ProductController extends Controller
{
    public function list_product(Request $request)
    {
        $admin = Admin::where('id', Session::get('id'))->first();
        if (isset($_GET['product_search'])) {
            $product_search = $_GET['product_search'];
            $products = Product::where('product_name', 'LIKE', "%{$product_search}%")->paginate(10);
            $products->appends($request->all());
            return view('admin-pages-content.products.list-product', compact('products', 'admin'));
        } else {
            $products = Product::where('deleted_at', NULL)->paginate(10);
            return view('admin-pages-content.products.list-product', compact('products', 'admin'));
        }
    }

    public function create_product(Request $request)
    {
        $request->validate([
            'product_name' => 'required',
            'product_slug' => 'required',
            'product_price' => 'required',
            'product_quantity' => 'required',
            'product_avatar' => 'required',
            'product_description' => 'required',
            'product_content' => 'required',
            'product_category_id' => 'required',
            'product_supplier_id' => 'required',
            'product_author_id' => 'required'
        ]);
        $files = $request->file('product_avatar');
        $formValue = $request->input();
        foreach ($files as $file) {
            $formValue['product_avatar'] = $file->getClientOriginalName();
            $file->storeAs('product-avatar', $file->getClientOriginalName(), 'product_avatar');
        }
        Product::create($formValue);
        return redirect('admin-page/products');
    }

    public function show_product($slug)
    {
        $product = Product::where('product_slug', '=', $slug)->first();
        $admin = Admin::where('id', Session::get('id'))->first();
        return view('admin-pages-content.products.show-product', compact('admin', 'product'));
    }

    public function edit_product($slug)
    {
        $suppliers = Supplier::all();
        $categories = Category::all();
        $admin = Admin::where('id', Session::get('id'))->first();
        $authors = Author::all();
        return view('admin-pages-content.products.edit-product', ['product' => Product::where('product_slug', '=', $slug)->first(), 'suppliers' => $suppliers, 'categories' => $categories, 'authors' => $authors, 'admin' => $admin]);
    }

    public function update_product(Request $request, $slug)
    {
        $request->validate([
            'product_name' => 'required',
            'product_slug' => 'required',
            'product_price' => 'required',
            'product_quantity' => 'required',
            'product_avatar' => 'required',
            'product_description' => 'required',
            'product_content' => 'required',
            'product_category_id' => 'required',
            'product_supplier_id' => 'required',
            'product_author_id' => 'required'
        ]);

        $product = Product::where('product_slug', '=', $slug)->first();
        $file = $request->file('product_avatar');
        $formValue = $request->input();
        $formValue['product_avatar'] = $file->getClientOriginalName();
        $file->storeAs('product-avatar', $file->getClientOriginalName(), 'product_avatar');
        $product->update($formValue);
        return redirect('admin-page/products');
    }

    public function delete_product($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return redirect('admin-page/products');
    }


    //gio hang

    public function addToCart($id)
    {

        $product = Product::find($id);
        $cart = session()->get('cart');
        if (isset($cart[$id])) {
            $cart[$id]['quantity'] = $cart[$id]['quantity'] + 1;
        } else {
            $cart[$id] = [
                'name' => $product->product_name,
                'price' => $product->product_price,
                'quantity' => 1,
                'image' => $product->product_avatar
            ];
        }
        session()->put('cart', $cart);
        return response()->json([
            'code' => 200,
            'message' => 'success',
            'status' => 200
        ]);
    }

    public function showCart()
    {
        $categories = Category::all();

        $carts = session()->get('cart');
        return view('cart.cart', compact('carts', 'categories'));
    }

    public function updateCart(Request $request)
    {
        if ($request->id && $request->quantity) {
            $carts = session()->get('cart');
            $carts[$request->id]['quantity'] = $request->quantity;
            session()->put('cart', $carts);
            $carts = session()->get('cart');
            $cart_component = view('cart.components.cart_component', compact('carts'))->render();
            return response()->json(['cart_component' => $cart_component, 'status' => 200]);
        }
    }

    public function deleteCart(Request $request)
    {
        if ($request->id) {
            $carts = session()->get('cart');
            unset($carts[$request->id]);
            session()->put('cart', $carts);
            $carts = session()->get('cart');
            $cart_component = view('cart.components.cart_component', compact('carts'))->render();
            return response()->json(['cart_component' => $cart_component, 'status' => 200]);
        }
    }
    public function saveAll(Request $request)
    {
    }

    public function home_page()
    {
        $categories = Category::all();
        $bestsells = Product::orderBy('product_sale', 'desc')->limit('6')->get();
        $newest = Product::orderBy('created_at', 'desc')->limit('4')->get();
        $products = Product::all();
        return view('user-pages-content.home-page', compact('products', 'categories', 'bestsells', 'newest'));
    }

    public function products_page()
    {
        $categories = Category::all();
        $products = Product::paginate(12);
        return view('user-pages-content.books-page', compact('products', 'categories'));
    }

    public function product_detail_page($slug)
    {

        $categories = Category::all();
        $product = Product::where('product_slug', '=', $slug)->first();
        return view('user-pages-content.books-detail-page', compact('product', 'categories'));
    }

    //payment
    public function search(Request $request)
    {
        $strToLower = strtolower($request->search);
        $searchString =  str_replace(' ', '+', $strToLower);
        $output = '';
        $products = Product::whereRaw("MATCH (product_name) AGAINST ('+$searchString' IN NATURAL MODE)")->get();
        foreach ($products as $key => $product) {
            $output .= '<tr>
            <td class="search-item" style="background-color: rgb(40 113 40);" ><p ><a href="/books/' . $product->product_slug . '">' . $product->product_name . '</a></p></td>
            </tr>';
        }
        return response()->json($output);
    }
}
