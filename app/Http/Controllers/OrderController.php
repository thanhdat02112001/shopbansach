<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use SebastianBergmann\Environment\Console;

class OrderController extends Controller
{
    public function pages_invoice()
    {
        $admin = Admin::where('id', Session::get('id'))->first();
        $orders = Order::where('order_status', '=', 0)->orderBy('id', 'desc')->paginate(4);

        return view('admin-pages-content.sales-page.pages-invoice', compact('admin', 'orders'));
    }

    public function page_projects(Request $request)
    {
        $admin = Admin::where('id', Session::get('id'))->first();
        if (isset($_GET['order_id_search'])) {
            $order_id_search = $_GET['order_id_search'];
            $orders = Order::where('order_status', '>', 0)->where("id", 'LIKE', "%{$order_id_search}%")->orderBy('id', 'desc')->paginate(10);
            $orders->appends($request->all());
        } else {
            $orders = Order::where('order_status', '>', 0)->orderBy('id', 'desc')->paginate(10);
        }
        return view('admin-pages-content.sales-page.page-projects', compact('admin', 'orders'));
    }

    public function update_status($id)
    {
        $order = Order::findOrFail($id);
        $order->update([
            'order_status' => 1
        ]);
        return response()->json($order);
    }

    public function update_status_success($id)
    {
        $order = Order::findOrFail($id);
        $money = Order::where('id', '=', $id)->value('total_money');
        $order->update([
            'order_status' => 2
        ]);
        return response()->json($order);
    }

    public function update_status_cancel($id)
    {
        $order = Order::findOrFail($id);
        $order->update([
            'order_status' => 3
        ]);
        return response()->json($order);
    }
}
