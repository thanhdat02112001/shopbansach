<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    public function sales_dashboard()
    {
        $admin = Admin::where('id', Session::get('id'))->first();
        $user = User::all();
        $orderSuccess = Order::where('order_status', 2)->get();
        $totalMoneyOrderSuccess = $orderSuccess->sum('total_money');
        $totals = Order::where('order_status', 2)->select(DB::raw("SUM(total_money) as sum"))
            ->whereYear('created_at', date('Y'))
            ->groupBy(DB::raw("Month(created_at)"))
            ->pluck('sum');
        $months = Order::where('order_status', 2)->select(DB::raw("Month(created_at) as month"))
            ->whereYear('created_at', date('Y'))
            ->groupBy(DB::raw("Month(created_at)"))
            ->pluck('month');
        $data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        $growthRate = 0;
        foreach ($months as $index => $month) {
            --$month;
            $data[$month] = $totals[$index];
            if ($month == 0) {
                $growthRate = 1;
            } else {
                $growthRate = $data[$month] / $data[$month - 1];
            }
        }
        return view('admin-pages-content.dashboard.dashboard-v1', compact('admin', 'user', 'orderSuccess', 'totalMoneyOrderSuccess', 'data', 'growthRate'));
    }

    public function orders_dashboard()
    {
        $admin = Admin::where('id', Session::get('id'))->first();
        $newOrder = Order::where('order_status', 0)->get();
        $successOrder = Order::where('order_status', 2)->get();
        $cancelOrder = Order::where('order_status', 3)->get();
        $totalOrder = Order::count('id');
        $totals = Order::select(DB::raw("COUNT(*) as count"))
            ->whereYear('created_at', date('Y'))
            ->groupBy(DB::raw("Month(created_at)"))
            ->pluck('count');
        $months = Order::select(DB::raw("Month(created_at) as month"))
            ->whereYear('created_at', date('Y'))
            ->groupBy(DB::raw("Month(created_at)"))
            ->pluck('month');
        $data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        foreach ($months as $index => $month) {
            --$month;
            $data[$month] = $totals[$index];
        }

        $totalSuccessOrder = Order::where('order_status', 2)->select(DB::raw("COUNT(*) as count"))
            ->whereYear('created_at', date('Y'))
            ->groupBy(DB::raw("Month(created_at)"))
            ->pluck('count');
        $monthSuccessOrder = Order::where('order_status', 2)->select(DB::raw("Month(created_at) as month"))
            ->whereYear('created_at', date('Y'))
            ->groupBy(DB::raw("Month(created_at)"))
            ->pluck('month');
        $data2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        foreach ($monthSuccessOrder as $index => $month) {
            --$month;
            $data2[$month] = $totalSuccessOrder[$index];
        }

        $totalCancelOrder = Order::where('order_status', 3)->select(DB::raw("COUNT(*) as count"))
            ->whereYear('created_at', date('Y'))
            ->groupBy(DB::raw("Month(created_at)"))
            ->pluck('count');
        $monthCancelOrder = Order::where('order_status', 3)->select(DB::raw("Month(created_at) as month"))
            ->whereYear('created_at', date('Y'))
            ->groupBy(DB::raw("Month(created_at)"))
            ->pluck('month');
        $data3 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        foreach ($monthCancelOrder as $index => $month) {
            --$month;
            $data3[$month] = $totalCancelOrder[$index];
        }
        return view('admin-pages-content.dashboard.dashboard-v2', compact('admin', 'newOrder', 'successOrder', 'cancelOrder', 'totalOrder', 'data', 'data2', 'data3'));
    }
}
