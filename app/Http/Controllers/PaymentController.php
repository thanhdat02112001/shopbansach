<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\User;
use Exception;
use Illuminate\Contracts\Session\Session as SessionSession;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class PaymentController extends Controller

{
    public function postPay(Request $request)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $data = $request->all();

        $user = session()->get('user');

        $data['transaction_user_id'] = $user['id'];
        $data['total_money'] = str_replace(',', '', $data['total']);
        $data['created_at'] = date('YmdHis');
        if ($request->httt_ma == 2) {
            $totalMoney = str_replace(",", "", $data['total']);
            session()->put('customer_information', $data);
            return view('vnpay.index', compact('totalMoney'));
        } else {
            $carts = session()->get('cart');

            $data['transaction_code'] = date('His');
            $transactionId = DB::table('transactions')->insertGetId(
                array(
                    'transaction_code' => $data['transaction_code'],
                    'transaction_email' => $data['kh_email'],
                    'transaction_phone' => $data['kh_dienthoai'],
                    'transaction_address' => $data['kh_diachi'],
                    'transaction_note' => $data['kh_note'],
                    'transaction_user_id' => $data['transaction_user_id'],
                    'created_at' => $data['created_at']

                )
            );

            if ($transactionId) {

                $orderId = DB::table('orders')->insertGetId(
                    [
                        'order_transaction_id' => $transactionId,
                        'created_at' => date('YmdHis'),
                        'order_status' => 0,
                        'total_money' => $data['total'],
                        'payment_type' => $data['httt_ma']
                    ]
                );

                foreach ($carts as $id => $cart) {
                    DB::table('order_detail')->insert(
                        [
                            'order_id' => $orderId,
                            'product_name' => $cart['name'],
                            'product_price' => $cart['price'],
                            'product_quantity' => $cart['quantity']
                        ]
                    );
                    $product_sale_old = DB::table('products')->where('id', $id)->value('product_sale');
                    $product_sale_new = $product_sale_old + $cart['quantity'];
                    DB::table('products')->where('id', $id)->update(
                        [
                            'product_sale' => $product_sale_new
                        ]
                    );
                    $cart_array[] = array(
                        'product_name' => $cart['name'],
                        'product_price' => $cart['price'],
                        'product_quantity' => $cart['quantity']
                    );
                }
            }
            // gửi mail
            $now = Carbon::now('Asia/Ho_Chi_Minh')->format('d-m-Y H:i:s');
            $orderIdFormat = date('dmY') . $orderId;
            $title_mail = "Đơn hàng xác nhận ngày " . $now;
            $shipping_array = array(
                'name' => $data['kh_ten'],
                'email' => $data['kh_email'],
                'phone' => $data['kh_dienthoai'],
                'address' => $data['kh_diachi'],
                'note' => $data['kh_note'],
                'payment_type' => $data['httt_ma'],
                'order_id' => $orderIdFormat,
            );


            Mail::send('mail.mail-order', ['carts' => $cart_array, 'shipping' => $shipping_array], function ($message) use ($title_mail, $data) {
                $message->to($data['kh_email'])->subject($title_mail);
                $message->from($data['kh_email'], $title_mail);
            });
            session()->forget('cart');
            return redirect('/')->with('success', 'Đơn hàng của bạn đã được lưu');
        }
    }
    public function createPayment(Request $request)
    {
        $vnp_TxnRef = $_POST['order_id']; //Mã đơn hàng. Trong thực tế Merchant cần insert đơn hàng vào DB và gửi mã này sang VNPAY
        $vnp_OrderInfo = $_POST['order_desc'];
        $vnp_OrderType = $_POST['order_type'];
        $vnp_Amount = $_POST['amount'] * 100;
        $vnp_Locale = $_POST['language'];
        $vnp_BankCode = $_POST['bank_code'];
        $vnp_IpAddr = $_SERVER['REMOTE_ADDR'];
        //Add Params of 2.0.1 Version
        // $vnp_ExpireDate = $_POST['txtexpire'];
        $inputData = array(
            "vnp_Version" => "2.1.0",
            "vnp_TmnCode" => env('VNP_TMN_CODE'),
            "vnp_Amount" => $vnp_Amount,
            "vnp_Command" => "pay",
            "vnp_CreateDate" => date('YmdHis'),
            "vnp_CurrCode" => "VND",
            "vnp_IpAddr" => $vnp_IpAddr,
            "vnp_Locale" => $vnp_Locale,
            "vnp_OrderInfo" => $vnp_OrderInfo,
            "vnp_OrderType" => $vnp_OrderType,
            "vnp_ReturnUrl" => route('vnpay.return'),
            "vnp_TxnRef" => $vnp_TxnRef,
            // "vnp_ExpireDate"=>$vnp_ExpireDate,
        );

        if (isset($vnp_BankCode) && $vnp_BankCode != "") {
            $inputData['vnp_BankCode'] = $vnp_BankCode;
        }


        //var_dump($inputData);
        ksort($inputData);
        $query = "";
        $i = 0;
        $hashdata = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashdata .= '&' . urlencode($key) . "=" . urlencode($value);
            } else {
                $hashdata .= urlencode($key) . "=" . urlencode($value);
                $i = 1;
            }
            $query .= urlencode($key) . "=" . urlencode($value) . '&';
        }

        $vnp_Url = env('VNP_URL') . "?" . $query;
        if (env('VNP_HASH_SECRET')) {
            $vnpSecureHash =   hash_hmac('sha512', $hashdata, env('VNP_HASH_SECRET')); //  
            $vnp_Url .= 'vnp_SecureHash=' . $vnpSecureHash;
        }
        $returnData = array(
            'code' => '00', 'message' => 'success', 'data' => $vnp_Url
        );
        if (isset($_POST['redirect'])) {
            header('Location: ' . $vnp_Url);
            die();
        } else {
            echo json_encode($returnData);
        }
    }
    public function sendmail()
    {
    }


    public function vnpayReturn(Request $request)
    {
        if (session()->has('customer_information') && $request->vnp_ResponseCode == '00') {
            $user = session()->get('user');
            $vnp_data = $request->all();
            $data = session()->get('customer_information');
            $carts = session()->get('cart');

            $transactionId = DB::table('transactions')->insertGetId(
                array(
                    'transaction_code' => $vnp_data['vnp_TransactionNo'],
                    'transaction_email' => $data['kh_email'],
                    'transaction_phone' => $data['kh_dienthoai'],
                    'transaction_address' => $data['kh_diachi'],
                    'transaction_note' => $data['kh_note'],
                    'transaction_user_id' => $data['transaction_user_id'],
                    'created_at' => date('YmdHis')

                )
            );

            if ($transactionId) {

                $orderId = DB::table('orders')->insertGetId(
                    [
                        'order_transaction_id' => $transactionId,
                        'created_at' => date('YmdHis'),
                        'order_status' => 0,
                        'total_money' => $data['total'],
                        'payment_type' => $data['httt_ma']
                    ]
                );

                foreach ($carts as $id => $cart) {
                    DB::table('order_detail')->insert(
                        [
                            'order_id' => $orderId,
                            'product_name' => $cart['name'],
                            'product_price' => $cart['price'],
                            'product_quantity' => $cart['quantity']
                        ]
                    );
                    $product_sale_old = DB::table('products')->where('id', $id)->value('product_sale');
                    $product_sale_new = $product_sale_old + $cart['quantity'];
                    DB::table('products')->where('id', $id)->update(
                        [
                            'product_sale' => $product_sale_new
                        ]
                    );
                    $cart_array[] = array(
                        'product_name' => $cart['name'],
                        'product_price' => $cart['price'],
                        'product_quantity' => $cart['quantity']
                    );
                }

                $dataPayment = [
                    'transaction_id' => $transactionId,
                    'user_id' => $user['id'],
                    'money' => $vnp_data['vnp_Amount'] / 100,
                    'vnp_response_code' => $vnp_data['vnp_ResponseCode'],
                    'code_vnpay' => $vnp_data['vnp_TransactionNo'],
                    'code_bank' => $vnp_data['vnp_BankCode'],
                    'time' => $vnp_data['vnp_PayDate'],

                ];

                DB::table('payments')->insert($dataPayment);
            }
            // gửi mail
            $now = Carbon::now('Asia/Ho_Chi_Minh')->format('d-m-Y H:i:s');
            $orderIdFormat = date('dmY') . $orderId;
            $title_mail = "Đơn hàng xác nhận ngày " . $now;
            $shipping_array = array(
                'name' => $data['kh_ten'],
                'email' => $data['kh_email'],
                'phone' => $data['kh_dienthoai'],
                'address' => $data['kh_diachi'],
                'note' => $data['kh_note'],
                'payment_type' => $data['httt_ma'],
                'order_id' => $orderIdFormat,
            );


            Mail::send('mail.mail-order', ['carts' => $cart_array, 'shipping' => $shipping_array], function ($message) use ($title_mail, $data) {
                $message->to($data['kh_email'])->subject($title_mail);
                $message->from($data['kh_email'], $title_mail);
            });
            Session::flash('success', 'Đơn hàng của bạn đã được lưu');
            session()->forget('cart');
            return view('vnpay.vnpay_return', compact('vnp_data'));
        } else {
            Session::flash('error', 'Đã xảy ra lỗi không thể thanh toán đơn hàng');
            return redirect('/');
        }
    }
}
