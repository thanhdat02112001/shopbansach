<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Author;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;

class AuthorController extends Controller
{
    public function list_author(Request $request)
    {
        $admin = Admin::where('id', Session::get('id'))->first();
        if (isset($_GET['author_search'])) {
            $author_search = $_GET['author_search'];
            $authors = Author::where('author_name', 'LIKE', "%{$author_search}%")->paginate(10);
            $authors->appends($request->all());
            return view('admin-pages-content.authors.list-author', compact('authors', 'admin'));
        } else {
            $authors = Author::where('deleted_at', NULL)->paginate(10);
            return view('admin-pages-content.authors.list-author', compact('authors', 'admin'));
        }
    }

    public function create_author(Request $request)
    {
        $request->validate([
            'author_name' => 'required',
            'author_avatar' => 'required',
            'author_biography' => 'required',
            'author_products' => 'required'
        ]);
        $file = $request->file('author_avatar');
        $formValue = $request->input();
        $formValue['author_avatar'] = $file->getClientOriginalName();
        $file->storeAs('author-avatar', $file->getClientOriginalName(), 'author_avatar');
        Author::create($formValue);
        return redirect('admin-page/authors');
    }

    public function show_author($id)
    {
        $admin = Admin::where('id', Session::get('id'))->first();
        $author = Author::findOrFail($id);
        return view('admin-pages-content.authors.show-author', compact('admin', 'author'));
    }

    public function edit_author($id)
    {
        $admin = Admin::where('id', Session::get('id'))->first();
        return view('admin-pages-content.authors.edit-author', ['author' => Author::findOrFail($id), 'admin' => $admin]);
    }

    public function update_author(Request $request, $id)
    {
        $request->validate([
            'author_name' => 'required',
            'author_avatar' => 'required',
            'author_biography' => 'required',
            'author_products' => 'required'
        ]);
        $author = Author::findOrFail($id);
        $file = $request->file('author_avatar');
        $formValue = $request->input();
        $formValue['author_avatar'] = $file->getClientOriginalName();
        $file->storeAs('author-avatar', $file->getClientOriginalName(), 'author_avatar');
        $author->update($formValue);
        return redirect('admin-page/authors');
    }

    public function delete_author($id)
    {
        $author = Author::where('id', $id)->first();
        $author->delete();
        return redirect('admin-page/authors');
    }

    public function showAuthor()
    {
        $authors = Author::paginate(12);
        $categories = Category::all();
        return view("user-pages-content.authors-page", compact('categories', 'authors'));
    }
    public function show_author_detail($id)
    {
        $author = Author::where('id', $id)->firstOrFail();
        $authorProducts = $author->products;
        $chunkAuthorProducts = $authorProducts->chunk(4);
        $categories = Category::all();

        return view("user-pages-content.authors-detail-page", compact("author", 'categories', 'chunkAuthorProducts'));
    }
}
