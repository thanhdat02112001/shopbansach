<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class AdminController extends Controller
{
    public function admin_register(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required | email | unique:admins,email',
            'password' => 'required',
            'retype_password' => 'required'
        ]);
        if ($request->input('password') == $request->input('retype_password')) {
            $formValue = [
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('password'))
            ];
            Admin::create($formValue);
            return redirect('admin-login');
        } else {
            $request->validate([
                'retype_password' => 'current_password'
            ]);
        }
    }

    public function admin_login(Request $request)
    {
        $request->validate([
            'email' => 'required | email',
            'password' => 'required'
        ]);
        $admin = Admin::where('email', $request->email)->first();
        if ($admin) {
            if (Hash::check($request->password, $admin->password)) {
                $request->session()->put('id', $admin->id);
                return redirect('sales-dashboard');
            } else {
                $request->validate([
                    'password' => 'current_password'
                ]);
            }
        } else {
            $request->validate([
                'password' => 'current_password'
            ]);
        }
    }

    public function password_forgot(Request $request)
    {
        $request->validate([
            'email' => 'required | email'
        ]);
        $email = DB::table('admins')->pluck('email');
        // dd($email[2]);
        for ($i = 0; $i < count($email); $i++) {
            if ($request->input('email') == $email[$i]) {
                $id = DB::table('admins')->where('email', "{$email[$i]}")->value('id');
                return redirect("$id/admin-login-password-reset");
            }
        }
        return view('layouts.admin-page-login-password-forgot');
    }

    public function password_reset(Request $request, $id)
    {
        $request->validate([
            'password' => 'required',
            'retype_password' => 'required'
        ]);
        if ($request->input('password') == $request->input('retype_password')) {
            $admin = Admin::findOrFail($id);
            $formValue = [
                'password' => Hash::make($request->input('password'))
            ];
            $admin->update($formValue);
            $request->session()->put('id', $admin->id);
            return redirect("sales-dashboard");
        } else {
            $request->validate([
                'retype_password' => 'current_password'
            ]);
        }
    }

    public function show_profile()
    {
        if (Session::has('id')) {
            $admin = Admin::where('id', Session::get('id'))->first();
        }
        return view('admin-pages-content.admin-profile.page-profile', compact('admin'));
    }

    public function update_profile(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('admins', 'email')->ignore(Session::get('id'))],
            'phone' => 'required',
            'address' => 'required',
            'dob' => 'required',
            'avatar' => 'required'
        ]);
        $admin = Admin::where('id', Session::get('id'))->first();
        $file = $request->file('avatar');
        $attribute = $request->input();
        $attribute['avatar'] = $file->getClientOriginalName();
        // dd($attribute['avatar']);
        $file->storeAs('admin-avatar', $file->getClientOriginalName(), 'admin_avatar');
        $admin->update($attribute);
        return redirect('admin-page/page-profile');
    }
}
