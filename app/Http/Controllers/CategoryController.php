<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Symfony\Component\Translation\MessageCatalogueInterface;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\Paginator;

class CategoryController extends Controller
{
    public function list_category(Request $request)
    {
        $admin = Admin::where('id', Session::get('id'))->first();
        if (isset($_GET['category_search'])) {
            $category_search = $_GET['category_search'];
            $categories = Category::where('category_name', 'LIKE', "%{$category_search}%")->paginate(10);
            $categories->appends($request->all());
            return view('admin-pages-content.categories.list-category', compact('categories', 'admin'));
        } else {
            $categories = Category::where('deleted_at', NULL)->paginate(10);
            return view('admin-pages-content.categories.list-category', compact('categories', 'admin'));
        }
    }

    public function create_category(Request $request)
    {
        $request->validate([
            'category_name' => 'required',
            'category_slug' => 'required',
            'category_description' => 'required'
        ]);
        Category::create($request->input());
        return redirect('admin-page/categories');
    }

    public function show_category($slug)
    {
        $admin = Admin::where('id', Session::get('id'))->first();
        $category = Category::where('category_slug', '=', $slug)->first();
        return view('admin-pages-content.categories.show-category', compact('category', 'admin'));
    }

    public function edit_category($slug)
    {
        $admin = Admin::where('id', Session::get('id'))->first();
        return view('admin-pages-content.categories.edit-category', ['category' => Category::where('category_slug', '=', $slug)->first(), 'admin' => $admin]);
    }

    public function update_category(Request $request, $slug)
    {
        $request->validate([
            'category_name' => 'required',
            'category_slug' => 'required',
            'category_description' => 'required'
        ]);
        $category = Category::where('category_slug', '=', $slug)->first();
        $formValue = $request->input();
        $category->update($formValue);
        return redirect('admin-page/categories');
    }

    public function delete_category($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();
        return redirect('admin-page/categories');
    }

    public function show_categories()
    {
        $categories = Category::all();

        return view('layouts.user-page-main', compact('categories'));
    }
    public function category_detail($id)
    {
        $categories = Category::all();
        $category = Category::findOrFail($id);
        $productCategories = $category->products()->paginate(12);
        return view('categories.category_detail', compact('category', 'categories', 'productCategories'));
    }
}
