<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\AuthorController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PaymentController;
use App\Models\Admin;
use App\Models\Author;
use App\Models\Category;
use App\Models\Order;
use App\Models\Product;
use App\Models\Supplier;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/cart', function () {
    return view('cart.cart');
});
Route::get('/checkout', function () {
    $categories = Category::all();
    $carts = session()->get('cart');
    return view('cart.checkout', compact('categories', 'carts'));
})->middleware('must-be-authenticated');


Route::get('/login', function () {
    $categories = Category::all();
    return view('user.login', compact('categories'));
});
Route::post('/login', [UserController::class, 'login']);

Route::get('/resigter', function () {
    $categories = Category::all();
    return view('user.resigter', compact('categories'));
});
Route::post('/resigter', [UserController::class, 'resigter']);

route::get('/logout', [UserController::class, 'logout']);

Route::get("/forgot-password", function () {
    return view("user.forgot_password");
});
Route::get("/change-pass", function () {
    return view("user.change_pass");
});

Route::get("/logout", function () {
    return redirect("/login");
});

Route::get('/', [ProductController::class, 'home_page'])->name('home');


//front-end
Route::get('/best-seller', [ProductController::class, 'best_sell']);
Route::get('/authors', [AuthorController::class, 'showAuthor'])->name('authors');


Route::get('/authors_detail/{id}', [AuthorController::class, "show_author_detail"]);

// Route::get("/list-categories",[CategoryController::class,'show_categories']);
Route::get("/category_detail/{id}", [CategoryController::class, "category_detail"]);

//them vao gio hang
Route::get("/add-to-cart/{id}", [ProductController::class, 'addToCart'])->name('addToCart');
Route::get("/showCart", [ProductController::class, 'showCart'])->name('showCart');
Route::get("/updateCart", [ProductController::class, 'updateCart'])->name('updateCart');
Route::get("/deleteCart", [ProductController::class, 'deleteCart'])->name('deleteCart');
Route::post('/save-all', [ProductController::class, 'saveAll']);
// thanh toan vnpay
Route::post('/payment/online', [PaymentController::class, 'createPayment'])->name('payment.online');
Route::post('/payment', [PaymentController::class, 'postPay'])->name('postPay');
Route::get('/vnpay/return', [PaymentController::class, 'vnpayReturn'])->name('vnpay.return');
// TIM KIEM
Route::get('/search', [ProductController::class, 'search'])->name('search');


Route::get('/books', [ProductController::class, 'products_page'])->name('books');

Route::get('/books/{id}', [ProductController::class, 'product_detail_page'])->name('product_detail');



Route::get('/comingsoon', function () {
    return view('comingsoon.comingsoon');
});

Route::get('/admin', function () {
    return view('layouts.admin-page-main');
});

//admin-pages
Route::get('sales-dashboard', [DashboardController::class, 'sales_dashboard'])->name('dashboard-v1')->middleware('isLoggedIn');

Route::get('orders-dashboard', [DashboardController::class, 'orders_dashboard'])->name('dashboard-v2')->middleware('isLoggedIn');

Route::get('create-category', function () {
    $admin = Admin::where('id', Session::get('id'))->first();
    return view('admin-pages-content.categories.create-category', compact('admin'));
})->name('create-category');
Route::post('create-category', [CategoryController::class, 'create_category']);

Route::get('create-product', function () {
    $authors = Author::all();
    $categories = Category::all();
    $admin = Admin::where('id', Session::get('id'))->first();
    $suppliers = Supplier::all();
    return view('admin-pages-content.products.create-product', compact('authors', 'categories', 'suppliers', 'admin'));
})->name('create-product');
Route::post('create-product', [ProductController::class, 'create_product']);

Route::get('create-author', function () {
    $admin = Admin::where('id', Session::get('id'))->first();
    return view('admin-pages-content.authors.create-author', compact('admin'));
})->name('create-author');
Route::post('create-author', [AuthorController::class, 'create_author']);

Route::get('create-supplier', function () {
    $admin = Admin::where('id', Session::get('id'))->first();
    return view('admin-pages-content.suppliers.create-supplier', compact('admin'));
})->name('create-supplier');
Route::post('create-supplier', [SupplierController::class, 'create_supplier']);

Route::get('admin-page/products', [ProductController::class, 'list_product'])->name('list-product');
Route::get('admin-page/product/{id}/edit', [ProductController::class, 'edit_product']);
Route::post('admin-page/product/{id}/edit', [ProductController::class, 'update_product']);
Route::get('admin-page/product/{id}/delete', [ProductController::class, 'delete_product']);
Route::get('admin-page/product/{id}', [ProductController::class, 'show_product']);

Route::get('admin-page/authors', [AuthorController::class, 'list_author'])->name('list-author');
Route::get('admin-page/author/{id}', [AuthorController::class, 'show_author']);
Route::get('admin-page/author/{id}/edit', [AuthorController::class, 'edit_author']);
Route::post('admin-page/author/{id}/edit', [AuthorController::class, 'update_author']);
Route::get('admin-page/author/{id}/delete', [AuthorController::class, 'delete_author']);


Route::get('admin-page/categories', [CategoryController::class, 'list_category'])->name('list-category');
Route::get('admin-page/category/{id}', [CategoryController::class, 'show_category'])->name('show-category');
Route::get('admin-page/category/{id}/edit', [CategoryController::class, 'edit_category']);
Route::post('admin-page/category/{id}/edit', [CategoryController::class, 'update_category']);
Route::get('admin-page/category/{id}/delete', [CategoryController::class, 'delete_category']);

Route::get('admin-page/suppliers', [SupplierController::class, 'list_supplier'])->name('list-supplier');
Route::get('admin-page/supplier/{id}/edit', [SupplierController::class, 'edit_supplier']);
Route::post('admin-page/supplier/{id}/edit', [SupplierController::class, 'update_supplier']);
Route::get('admin-page/supplier/{id}/delete', [SupplierController::class, 'delete_supplier']);
Route::get('admin-page/supplier/{id}', [SupplierController::class, 'show_supplier']);


Route::get('admin-page/page-profile', [AdminController::class, 'show_profile'])->name('page-profile');
Route::post('admin-page/page-profile', [AdminController::class, 'update_profile']);

Route::get('admin-page/pages-invoice', [OrderController::class, 'pages_invoice'])->name('pages-invoice');
Route::post('admin-page/order/{id}/update-status', [OrderController::class, 'update_status']);
Route::post('admin-page/order/{id}/update-status-success', [OrderController::class, 'update_status_success']);
Route::post('admin-page/order/{id}/update-status-cancel', [OrderController::class, 'update_status_cancel']);

Route::get('admin-page/page-projects', [OrderController::class, 'page_projects'])->name('page-projects');

Route::get('admin-login', function () {
    return view('layouts.admin-page-login');
})->name('admin-login');
Route::post('admin-login', [AdminController::class, 'admin_login']);

Route::get('admin-login-password-forgot', function () {
    return view('layouts.admin-page-login-password-forgot');
})->name('admin-login-forgot');
Route::post('admin-login-password-forgot', [AdminController::class, 'password_forgot']);

Route::get('{id}/admin-login-password-reset', function () {
    return view('layouts.admin-page-login-reset-password');
});
Route::post('{id}/admin-login-password-reset', [AdminController::class, 'password_reset']);

Route::get('admin-login-register', function () {
    return view('layouts.admin-page-login-register');
})->name('admin-login-register');
Route::post('admin-login-register', [AdminController::class, 'admin_register']);
