@extends('layouts.user-page-main')
@section('content')
    <!--************************************
         Best Selling Start
       *************************************-->

    <section class="tg-sectionspace tg-haslayout">
        <div class="container">
            <div class="row">
                <div style="margin-top:0px; text-align:center; display:flex; justify-content:center">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (session('notice'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('notice') }}
                        </div>
                    @endif
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-sectionhead">
                        <h2><span> Những loại sách</span> bán chạy nhất</h2>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div id="tg-bestsellingbooksslider" class="tg-bestsellingbooksslider tg-bestsellingbooks owl-carousel">
                        @foreach ($bestsells as $product)
                            <div class="item">
                                <div class="tg-postbook">
                                    <figure class="tg-featureimg" style="width:150px; height:200px">
                                        <div class="tg-bookimg">
                                            <input type="hidden" id="wistlist_image_{{ $product->id }}"
                                                value="{{ Storage::disk('product_avatar')->url($product->product_avatar) }}">

                                            <div class="tg-frontcover"><img
                                                    src="{{ Storage::disk('product_avatar')->url($product->product_avatar) }}"
                                                    alt="image description"></div>
                                            <div class="tg-backcover"><img
                                                    src="{{ Storage::disk('product_avatar')->url($product->product_avatar) }}"
                                                    alt="image description"></div>
                                        </div>
                                        <a class="tg-btnaddtowishlist" href="javascript:void(0);" id="{{ $product->id }}"
                                            onclick="add_wistlist(this.id)">
                                            <i class="icon-heart"></i>
                                            <span>Yêu thích</span>
                                        </a>
                                    </figure>
                                    <div class="tg-postbookcontent">
                                        <ul class="tg-bookscategories">
                                            <li><a href="javascript:void(0);">{{ $product->category->category_name }}</a>
                                            </li>
                                        </ul>
                                        <div class="tg-booktitle">
                                            <h3 style="height: 50px"><a
                                                    href="/books/{{ $product->product_slug }}">{{ $product->product_name }}</a>
                                            </h3>
                                            <input type="hidden" id="wistlist_name_{{ $product->id }}"
                                                value="{{ $product->product_name }}">
                                            <input type="hidden" id="wistlist_url_{{ $product->id }}"
                                                value="{{ route('product_detail', $product->id) }}">
                                        </div>
                                        <span class="tg-bookwriter">Tác giả:<a
                                                href="/authors_detail/{{ $product->author->id }}">{{ $product->author->author_name }}</a></span>
                                        <span class="tg-bookwriter"><span>Đã bán:
                                                {{ $product->product_sale }}</span></span>
                                        <span class="tg-bookprice">
                                            <ins>{{ number_format($product->product_price) }}<sup
                                                    style="font-size: 12px">vnđ</sup></ins>
                                            <input type="hidden" id="wistlist_price_{{ $product->id }}"
                                                value="{{ $product->product_price }}">
                                        </span>
                                        <a class="tg-btn tg-btnstyletwo add_to_cart" href="javascript:void(0);"
                                            data-url="{{ route('addToCart', ['id' => $product->id]) }}"
                                            style="text-align: bottom">
                                            <i class="fa fa-shopping-basket"></i>
                                            <em>Mua hàng</em>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach




                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--************************************
         Best Selling End
       *************************************-->

    <!--************************************
         Featured Item Start
       *************************************-->

    <!--************************************
         Featured Item End
       *************************************-->
    <!--************************************
         New Release Start
       *************************************-->
    <section class="tg-sectionspace tg-haslayout">
        <div class="container">
            <div class="row">
                <div class="tg-newrelease">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="tg-sectionhead">
                            <h2>Những loại sách mới</h2>
                        </div>
                        <div class="tg-description">
                            <p>Chúng tôi luôn thay đổi và cập nhật các sản phẩm mới. Nhằm tăng cao nhu cầu tìm kiếm và đáp
                                ứng được sở thích của bạn đọc. Chúc bạn đọc tìm được những sản phẩm mình yêu thích và có tâm
                                trạng thật tốt khi đọc sách. </p>
                        </div>
                        <div class="tg-btns">
                            <a class="tg-btn tg-active" href="/books">Xem tất cả</a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="row">
                            <form action="">
                                @csrf
                                <div class="tg-newreleasebooks">
                                    @foreach ($newest as $product)
                                        <div class="col-xs-4 col-sm-4 col-md-6 col-lg-4">
                                            <div class="tg-postbook">
                                                <figure class="tg-featureimg" style="width:150px; height:200px">
                                                    <div class="tg-bookimg">
                                                        <input type="hidden" id="wistlist_image_{{ $product->id }}"
                                                            value="{{ Storage::disk('product_avatar')->url($product->product_avatar) }}">
                                                        <div class="tg-frontcover"><img
                                                                src="{{ Storage::disk('product_avatar')->url($product->product_avatar) }}"
                                                                alt="image description"></div>
                                                        <div class="tg-backcover"><img
                                                                src="{{ Storage::disk('product_avatar')->url($product->product_avatar) }}">
                                                        </div>
                                                    </div>
                                                    <a class="tg-btnaddtowishlist" href="javascript:void(0);"
                                                        id="{{ $product->id }}" onclick="add_wistlist(this.id)">
                                                        <i class="icon-heart"></i>
                                                        <span>Yêu thích</span>
                                                    </a>
                                                </figure>
                                                <div class="tg-postbookcontent">
                                                    <ul class="tg-bookscategories">
                                                        <li><a
                                                                href="javascript:void(0);">{{ $product->category->category_name }}</a>
                                                        </li>

                                                    </ul>
                                                    <div class="tg-booktitle">
                                                        <h3 style="height: 48px">
                                                            <a
                                                                href="/books/{{ $product->product_slug }}">{{ $product->product_name }}</a>
                                                            <input type="hidden" id="wistlist_name_{{ $product->id }}"
                                                                value="{{ $product->product_name }}">
                                                            <input type="hidden" id="wistlist_url_{{ $product->id }}"
                                                                value="{{ route('product_detail', $product->id) }}">
                                                        </h3>
                                                    </div>
                                                    <span class="tg-bookwriter">Tác giả:<a
                                                            href="/authors_detail/{{ $product->author->id }}">{{ $product->author->author_name }}</a></span>
                                                    <span class="tg-bookwriter"><span>Đã bán:
                                                            {{ $product->product_sale }}</span></span>

                                                    <input type="hidden" id="wistlist_price_{{ $product->id }}"
                                                        value="{{ $product->product_price }}">
                                                    <span class="tg-bookprice" style="padding-top: 10px;">
                                                        <ins>{{ number_format($product->product_price) }}<sup
                                                                style="font-size: 12px">vnđ</sup></ins>
                                                    </span>
                                                    <a class="tg-btn tg-btnstyletwo add_to_cart" href="javascript:void(0);"
                                                        data-url="{{ route('addToCart', ['id' => $product->id]) }}"
                                                        style="text-align: bottom">
                                                        <i class="fa fa-shopping-basket"></i>
                                                        <em>Mua hàng</em>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--************************************
         New Release End
       *************************************-->
    <!--************************************
         Collection Count Start
       *************************************-->

    @include('cart.add-to-cart')
@endsection
