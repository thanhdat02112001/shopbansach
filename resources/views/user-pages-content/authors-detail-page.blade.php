@extends('layouts.user-page-main')
@section('content')
    <div class="tg-innerbanner tg-haslayout tg-parallax tg-bginnerbanner" data-z-index="-100" data-appear-top-offset="600"
        data-parallax="scroll" data-image-src="images/parallax/bgparallax-07.jpg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-innerbannercontent">
                        <h1>Tiểu sử tác giả</h1>
                        <ol class="tg-breadcrumb">
                            <li><a href="{{ route('home') }}">Trang chủ</a></li>
                            <li><a href="{{ route('authors') }}">Tác giả</a></li>
                            <li class="tg-active">Tiểu sử tác giả</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <main id="tg-main" class="tg-main tg-haslayout">
        <!--************************************
                Author Detail Start
        *************************************-->
        <div class="tg-sectionspace tg-haslayout">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-authordetail">
                            <div class="row">
                                <div class="col-md-4">
                                    <figure class="tg-authorimg">
                                        <img src="{{ Storage::disk('author_avatar')->url($author->author_avatar) }}"
                                            alt="">
                                    </figure>
                                </div>
                                <div class="col-md-8">
                                    <div class="tg-authorcontentdetail">
                                        <div class="tg-sectionhead">
                                            <h2>{{ $author->author_name }}</h2>
                                        </div>
                                        <div class="tg-description">
                                            <p>{{ $author->author_biography }}</p>
                                        </div>
                                        <div class="tg-booksfromauthor">
                                            <div class="tg-sectionhead">
                                                <h2>Sách của {{ $author->author_name }}</h2>
                                            </div>
                                            @foreach ($chunkAuthorProducts as $chunkAuthorProduct)
                                                <div class="row">
                                                    @foreach ($chunkAuthorProduct as $product)
                                                        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
                                                            <div class="tg-postbook">
                                                                <figure class="tg-featureimg"
                                                                    style="width:150px; height:200px">
                                                                    <div class="tg-bookimg">
                                                                        <input type="hidden"
                                                                            id="wistlist_image_{{ $product->id }}"
                                                                            value="{{ Storage::disk('product_avatar')->url($product->product_avatar) }}">
                                                                        <div class="tg-frontcover"><img
                                                                                src="{{ Storage::disk('product_avatar')->url($product->product_avatar) }}"
                                                                                alt="image description"></div>
                                                                        <div class="tg-backcover"><img
                                                                                src="{{ Storage::disk('product_avatar')->url($product->product_avatar) }}"
                                                                                alt="image description"></div>
                                                                    </div>
                                                                    <a class="tg-btnaddtowishlist"
                                                                        href="javascript:void(0);" id="{{ $product->id }}"
                                                                        onclick="add_wistlist(this.id)">
                                                                        <i class="icon-heart"></i>
                                                                        <span>Yêu thích</span>
                                                                    </a>
                                                                </figure>
                                                                <div class="tg-postbookcontent">
                                                                    <ul class="tg-bookscategories">
                                                                        <li><a
                                                                                href="/category_detail/{{ $product->category->id }}">{{ $product->category->category_name }}</a>
                                                                        </li>
                                                                    </ul>
                                                                    <div class="tg-themetagbox"><span
                                                                            class="tg-themetag">sale</span></div>
                                                                    <div class="tg-booktitle">
                                                                        <h3><a
                                                                                href="/books/{{ $product->id }}">{{ $product->product_name }}</a>
                                                                        </h3>
                                                                        <input type="hidden"
                                                                            id="wistlist_name_{{ $product->id }}"
                                                                            value="{{ $product->product_name }}">
                                                                        <input type="hidden"
                                                                            id="wistlist_url_{{ $product->id }}"
                                                                            value="{{ route('product_detail', $product->id) }}">
                                                                    </div>

                                                                    <span class="tg-bookwriter"><span>Đã bán:
                                                                            {{ $product->product_sale }}</span></span>
                                                                    <span class="tg-bookprice">
                                                                        <input type="hidden"
                                                                            id="wistlist_price_{{ $product->id }}"
                                                                            value="{{ $product->product_price }}">
                                                                        <ins>{{ number_format($product->product_price) }}<sup
                                                                                style="font-size: 12px">vnđ</sup></ins>
                                                                    </span>
                                                                    <a class="tg-btn tg-btnstyletwo add_to_cart" href=""
                                                                        data-url="{{ route('addToCart', ['id' => $product->id]) }}">
                                                                        <i class="fa fa-shopping-basket"></i>
                                                                        <em>Mua hàng</em>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--************************************
                Author Detail End
        *************************************-->
    </main>
    @include('cart.add-to-cart');
@endsection
