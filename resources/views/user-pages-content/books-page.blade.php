@extends('layouts.user-page-main')
@section('content')
    <div class="tg-innerbanner tg-haslayout tg-parallax tg-bginnerbanner" data-z-index="-100" data-appear-top-offset="600"
        data-parallax="scroll" data-image-src="images/parallax/bgparallax-07.jpg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-innerbannercontent">
                        <h1>Sách</h1>
                        <ol class="tg-breadcrumb">
                            <li><a href="/home-page">Trang chủ</a></li>
                            <li class="tg-active">Sách</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="tg-sectionspace tg-haslayout">
            <div class="container">
                <div class="row">
                    <div id="tg-twocolumns" class="tg-twocolumns">
                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 pull-right">
                            <div id="tg-content" class="tg-content">
                                <div class="tg-products">
                                    <div class="tg-sectionhead">
                                        <h2><span>Dành cho bạn đọc</span>Tất cả sách của cửa hàng</h2>
                                    </div>

                                    <div class="tg-productgrid">
                                        @foreach ($products as $product)
                                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
                                                <div class="tg-postbook">
                                                    <figure class="tg-featureimg" style="width:150px; height:200px">
                                                        <div class="tg-bookimg">
                                                            <input type="hidden" id="wistlist_image_{{ $product->id }}"
                                                                value="{{ Storage::disk('product_avatar')->url($product->product_avatar) }}">
                                                            <div class="tg-frontcover"><img
                                                                    src="{{ Storage::disk('product_avatar')->url($product->product_avatar) }}"
                                                                    alt="image description"></div>
                                                            <div class="tg-backcover"><img
                                                                    src="{{ Storage::disk('product_avatar')->url($product->product_avatar) }}"
                                                                    alt="image description"></div>
                                                        </div>
                                                        <a class="tg-btnaddtowishlist" href="javascript:void(0);"
                                                            id="{{ $product->id }}" onclick="add_wistlist(this.id)">
                                                            <i class="icon-heart"></i>
                                                            <span>Yêu thích</span>
                                                        </a>
                                                    </figure>
                                                    <div class="tg-postbookcontent">
                                                        <ul class="tg-bookscategories">
                                                            <li><a
                                                                    href="javascript:void(0);">{{ $product->category->category_name }}</a>
                                                            </li>
                                                        </ul>

                                                        <div class="tg-booktitle">
                                                            <h3 style="height: 50px"><a
                                                                    href="/books/{{ $product->product_slug }}">{{ $product->product_name }}</a>
                                                            </h3>
                                                            <input type="hidden" id="wistlist_name_{{ $product->id }}"
                                                                value="{{ $product->product_name }}">
                                                            <input type="hidden" id="wistlist_url_{{ $product->id }}"
                                                                value="{{ route('product_detail', $product->id) }}">
                                                        </div>
                                                        <span class="tg-bookwriter">Tác giả: <a
                                                                href="authors_detail/{{ $product->author->id }}">{{ $product->author->author_name }}</a></span>
                                                        <span class="tg-bookwriter"><span>Đã bán:
                                                                {{ $product->product_sale }}</span></span>
                                                        <span class="tg-bookprice">
                                                            <input type="hidden" id="wistlist_price_{{ $product->id }}"
                                                                value="{{ $product->product_price }}">
                                                            <ins style="padding-top: 10px">{{ number_format($product->product_price) }}<sup
                                                                    style="font-size: 12px">vnđ</sup></ins>
                                                        </span>
                                                        <a class="tg-btn tg-btnstyletwo add_to_cart" href=""
                                                            data-url="{{ route('addToCart', ['id' => $product->id]) }}">
                                                            <i class="fa fa-shopping-basket"></i>
                                                            <em>Thêm giỏ hàng</em>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="row">
                                        <div class="col-12" style="margin-left: 15px">
                                            {{ $products->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @include('user-layouts.sidebar-menu')
                    </div>
                </div>
            </div>
        </div>

    </main>


    @include('cart.add-to-cart')
@endsection
