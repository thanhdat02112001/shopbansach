@extends('layouts.user-page-main')

@section('content')
    <div class="tg-innerbanner tg-haslayout tg-parallax tg-bginnerbanner" data-z-index="-100" data-appear-top-offset="600"
        data-parallax="scroll" data-image-src="images/parallax/bgparallax-07.jpg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-innerbannercontent">
                        <h1>Chi tiết sách</h1>
                        <ol class="tg-breadcrumb">
                            <li><a href="/home">Trang chủ</a></li>
                            <li><a href="/books">Sách</a></li>
                            <li class="tg-active">Chi tiết sách</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <main id="tg-main" class="tg-main tg-haslayout">
        <!--************************************
                News Grid Start
        *************************************-->
        <div class="tg-sectionspace tg-haslayout">
            <div class="container">
                <div class="row">
                    <div id="tg-twocolumns" class="tg-twocolumns">
                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 pull-right">
                            <div id="tg-content" class="tg-content">
                                <div class="tg-productdetail">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                            <div class="tg-postbook">
                                                <figure class="tg-featureimg"><img
                                                        src="{{ Storage::disk('product_avatar')->url($product->product_avatar) }}"
                                                        alt="image description" style="width: 100%; height:100%"></figure>
                                                <div class="tg-postbookcontent">

                                                    <span class="tg-bookwriter"></span>
                                                    <a class="tg-btn tg-active tg-btn-lg add_to_cart" href=""
                                                        data-url="{{ route('addToCart', ['id' => $product->id]) }}">Thêm vào
                                                        giỏ hàng</a>
                                                    <a class="tg-btnaddtowishlist" href="javascript:void(0);"
                                                        id="{{ $product->id }}" onclick="add_wistlist(this.id)">
                                                        <span>Thêm vào yêu thích</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                            <div class="tg-productcontent">
                                                <ul class="tg-bookscategories">
                                                    <li><a
                                                            href="javascript:void(0);">{{ $product->category->category_name }}</a>
                                                    </li>
                                                </ul>

                                                <div class="tg-booktitle">
                                                    <h3>{{ $product->product_name }}</h3>
                                                </div>
                                                <input type="hidden" id="wistlist_price_{{ $product->id }}"
                                                    value="{{ $product->product_price }}">
                                                <input type="hidden" id="wistlist_name_{{ $product->id }}"
                                                    value="{{ $product->product_name }}">
                                                <input type="hidden" id="wistlist_url_{{ $product->id }}"
                                                    value="{{ route('product_detail', $product->id) }}">
                                                <input type="hidden" id="wistlist_image_{{ $product->id }}"
                                                    value="{{ Storage::disk('product_avatar')->url($product->product_avatar) }}">
                                                <ul class="tg-productinfo">
                                                    <li><span>Mô tả</span><span>{{ $product->product_description }}</span>
                                                    </li>
                                                    <li><span>Tóm tắt nội
                                                            dung</span><span>{{ $product->product_content }}</span></li>
                                                    <li><span>Nhà xuất
                                                            bản:</span><span>{{ $product->supplier->supplier_name }}</span>
                                                    </li>
                                                    <li><span>Giá
                                                            tiền</span><span>{{ number_format($product->product_price) }}
                                                            VND</span></li>
                                                </ul>

                                            </div>
                                        </div>
                                        <div class="tg-aboutauthor" style="margin-top:50px">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="tg-sectionhead">
                                                    <h2>Về tác giả</h2>
                                                </div>
                                                <div class="tg-authorbox">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <figure class="tg-authorimg">
                                                                <img src="{{ Storage::disk('author_avatar')->url($product->author->author_avatar) }}"
                                                                    alt="">
                                                            </figure>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="tg-authorinfo">
                                                                <div class="tg-authorhead">
                                                                    <div class="tg-leftarea">
                                                                        <div class="tg-authorname">
                                                                            <h2>{{ $product->author->author_name }}</h2>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="tg-description">
                                                                    <p>{{ $product->author->author_biography }}</p>
                                                                </div>
                                                                <a class="tg-btn tg-active"
                                                                    href="/authors_detail/{{ $product->author->id }}">Xem
                                                                    thêm </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @include('user-layouts.sidebar-menu')
                    </div>
                </div>
            </div>
        </div>
        <!--************************************
                News Grid End
        *************************************-->
    </main>
    @include('cart.add-to-cart')
@endsection
