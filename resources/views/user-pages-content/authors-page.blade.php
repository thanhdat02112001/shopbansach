@extends('layouts.user-page-main')
@section('content')
    <div class="tg-innerbanner tg-haslayout tg-parallax tg-bginnerbanner" data-z-index="-100" data-appear-top-offset="600"
        data-parallax="scroll" data-image-src="images/parallax/bgparallax-07.jpg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-innerbannercontent">
                        <h1>Tác giả</h1>
                        <ol class="tg-breadcrumb">
                            <li><a href="{{ route('home') }}">Trang chủ</a></li>
                            <li class="tg-active">Tác giả</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <main id="tg-main" class="tg-main tg-haslayout">
        <!--************************************
                Authors Start
        *************************************-->
        <div class="tg-authorsgrid">
            <div class="container">
                <div class="row">
                    <div class="tg-authors">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="tg-sectionhead">
                                <h2>Các tác giả</h2>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        @foreach ($authors as $author)
                            <div class="card col-xs-6 col-sm-4 col-md-3 col-lg-2" style="width: 18rem; height:38rem">
                                <img style="margin-top: 15px; height: 180px" class="card-img-top"
                                    src="{{ Storage::disk('author_avatar')->url($author->author_avatar) }}"
                                    alt="author-image">
                                <div class="card-body">
                                    <h5 class="card-title" style="width:100%">{{ $author->author_name }}</h5>
                                    <p class="card-text">Sở hữu {{ count($author->products) }} sách</p>
                                    <a href="/authors_detail/{{ $author->id }}" class="btn btn-success">Xem chi tiết</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <div class="col-12" style="margin-left: 15px">
                            {{ $authors->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--************************************
                Authors End
        *************************************-->
        <!--************************************
                Testimonials Start
        *************************************-->
        <section class="tg-parallax tg-bgtestimonials tg-haslayout" data-z-index="-100" data-appear-top-offset="600"
            data-parallax="scroll" data-image-src="images/parallax/bgparallax-05.jpg">
            <div class="tg-sectionspace tg-haslayout">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-push-2">
                            <div id="tg-testimonialsslider" class="tg-testimonialsslider tg-testimonials owl-carousel">
                                @foreach ($authors as $author)
                                    <div class="item tg-testimonial">
                                        <figure><img
                                                src="{{ Storage::disk('author_avatar')->url($author->author_avatar) }}"
                                                alt="author-image"></figure>
                                        <blockquote><q>{{ $author->author_biography }}</q></blockquote>
                                        <div class="tg-testimonialauthor">
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--************************************
                Testimonials End
        *************************************-->
        <!--************************************
                Picked By Author Start
        *************************************-->

        {{-- <section class="tg-sectionspace tg-haslayout">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-sectionhead">
                        <h2><span>Một số cuốn sách hay</span>được lựa chọn bởi tác giả</h2>
                        <a class="tg-btn" href="javascript:void(0);">View All</a>
                    </div>
                </div>
                <div id="tg-pickedbyauthorslider" class="tg-pickedbyauthor tg-pickedbyauthorslider owl-carousel">
                    <div class="item">
                        <div class="tg-postbook">
                            <figure class="tg-featureimg">
                                <div class="tg-bookimg">
                                    <div class="tg-frontcover"><img src="images/books/img-10.jpg" alt="image description"></div>
                                </div>
                                <div class="tg-hovercontent">
                                    <div class="tg-description">
                                        <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt labore toloregna aliqua enim adia minim veniam, quis nostrud.</p>
                                    </div>
                                    <strong class="tg-bookpage">Book Pages: 206</strong>
                                    <strong class="tg-bookcategory">Category: Adventure, Fun</strong>
                                    <strong class="tg-bookprice">Price: $23.18</strong>
                                    <div class="tg-ratingbox"><span class="tg-stars"><span></span></span></div>
                                </div>
                            </figure>
                            <div class="tg-postbookcontent">
                                <div class="tg-booktitle">
                                    <h3><a href="javascript:void(0);">Seven Minutes In Heaven</a></h3>
                                </div>
                                <span class="tg-bookwriter">By: <a href="javascript:void(0);">Sunshine Orlando</a></span>
                                <a class="tg-btn tg-btnstyletwo" href="javascript:void(0);">
                                    <i class="fa fa-shopping-basket"></i>
                                    <em>Add To Basket</em>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="tg-postbook">
                            <figure class="tg-featureimg">
                                <div class="tg-bookimg">
                                    <div class="tg-frontcover"><img src="images/books/img-11.jpg" alt="image description"></div>
                                </div>
                                <div class="tg-hovercontent">
                                    <div class="tg-description">
                                        <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt labore toloregna aliqua enim adia minim veniam, quis nostrud.</p>
                                    </div>
                                    <strong class="tg-bookpage">Book Pages: 206</strong>
                                    <strong class="tg-bookcategory">Category: Adventure, Fun</strong>
                                    <strong class="tg-bookprice">Price: $23.18</strong>
                                    <div class="tg-ratingbox"><span class="tg-stars"><span></span></span></div>
                                </div>
                            </figure>
                            <div class="tg-postbookcontent">
                                <div class="tg-booktitle">
                                    <h3><a href="javascript:void(0);">Slow And Steady Wins The Race</a></h3>
                                </div>
                                <span class="tg-bookwriter">By: <a href="javascript:void(0);">Drusilla Glandon</a></span>
                                <a class="tg-btn tg-btnstyletwo" href="javascript:void(0);">
                                    <i class="fa fa-shopping-basket"></i>
                                    <em>Add To Basket</em>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="tg-postbook">
                            <figure class="tg-featureimg">
                                <div class="tg-bookimg">
                                    <div class="tg-frontcover"><img src="images/books/img-12.jpg" alt="image description"></div>
                                </div>
                                <div class="tg-hovercontent">
                                    <div class="tg-description">
                                        <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt labore toloregna aliqua enim adia minim veniam, quis nostrud.</p>
                                    </div>
                                    <strong class="tg-bookpage">Book Pages: 206</strong>
                                    <strong class="tg-bookcategory">Category: Adventure, Fun</strong>
                                    <strong class="tg-bookprice">Price: $23.18</strong>
                                    <div class="tg-ratingbox"><span class="tg-stars"><span></span></span></div>
                                </div>
                            </figure>
                            <div class="tg-postbookcontent">
                                <div class="tg-booktitle">
                                    <h3><a href="javascript:void(0);">There’s No Time Like The Present</a></h3>
                                </div>
                                <span class="tg-bookwriter">By: <a href="javascript:void(0);">Patrick Seller</a></span>
                                <a class="tg-btn tg-btnstyletwo" href="javascript:void(0);">
                                    <i class="fa fa-shopping-basket"></i>
                                    <em>Add To Basket</em>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="tg-postbook">
                            <figure class="tg-featureimg">
                                <div class="tg-bookimg">
                                    <div class="tg-frontcover"><img src="images/books/img-10.jpg" alt="image description"></div>
                                </div>
                                <div class="tg-hovercontent">
                                    <div class="tg-description">
                                        <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt labore toloregna aliqua enim adia minim veniam, quis nostrud.</p>
                                    </div>
                                    <strong class="tg-bookpage">Book Pages: 206</strong>
                                    <strong class="tg-bookcategory">Category: Adventure, Fun</strong>
                                    <strong class="tg-bookprice">Price: $23.18</strong>
                                    <div class="tg-ratingbox"><span class="tg-stars"><span></span></span></div>
                                </div>
                            </figure>
                            <div class="tg-postbookcontent">
                                <div class="tg-booktitle">
                                    <h3><a href="javascript:void(0);">Seven Minutes In Heaven</a></h3>
                                </div>
                                <span class="tg-bookwriter">By: <a href="javascript:void(0);">Sunshine Orlando</a></span>
                                <a class="tg-btn tg-btnstyletwo" href="javascript:void(0);">
                                    <i class="fa fa-shopping-basket"></i>
                                    <em>Add To Basket</em>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="tg-postbook">
                            <figure class="tg-featureimg">
                                <div class="tg-bookimg">
                                    <div class="tg-frontcover"><img src="images/books/img-11.jpg" alt="image description"></div>
                                </div>
                                <div class="tg-hovercontent">
                                    <div class="tg-description">
                                        <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt labore toloregna aliqua enim adia minim veniam, quis nostrud.</p>
                                    </div>
                                    <strong class="tg-bookpage">Book Pages: 206</strong>
                                    <strong class="tg-bookcategory">Category: Adventure, Fun</strong>
                                    <strong class="tg-bookprice">Price: $23.18</strong>
                                    <div class="tg-ratingbox"><span class="tg-stars"><span></span></span></div>
                                </div>
                            </figure>
                            <div class="tg-postbookcontent">
                                <div class="tg-booktitle">
                                    <h3><a href="javascript:void(0);">Slow And Steady Wins The Race</a></h3>
                                </div>
                                <span class="tg-bookwriter">By: <a href="javascript:void(0);">Drusilla Glandon</a></span>
                                <a class="tg-btn tg-btnstyletwo" href="javascript:void(0);">
                                    <i class="fa fa-shopping-basket"></i>
                                    <em>Add To Basket</em>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> --}}

        <!--************************************
                Picked By Author End
        *************************************-->
    </main>
@endsection
