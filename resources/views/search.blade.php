<script src="https://code.jquery.com/jquery-3.6.0.min.js"
integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
        function search() {
            let search = $('#search').val();
            $.ajax({
                method: 'get',
                url: "{{ route('search') }}",
                data: {
                    'search': search
                },
                dataType: 'json',
                success: function(response) {
                    $("#search-result").html(response);
                }
            });
        }

        function debounce(func, wait, immediate) {
            var timeout;
            return function() {
                var context = this,
                    args = arguments;
                var later = function() {
                    timeout = null;
                    if (!immediate) func.apply(context, args);
                };
                var callNow = immediate && !timeout;
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
                if (callNow) func.apply(context, args);
            };
        };
        $("#search").keyup(debounce(function() {
            search();
        }, 300));

    });
</script>
