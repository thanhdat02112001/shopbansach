<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
    function view(){
        if(localStorage.getItem('data')!=null){
            let data= JSON.parse(localStorage.getItem('data'));
            data.reverse(); // day cai moi nhat len dau
            // $('#row_wistlist').style.overflow= 'scroll';
            // $("#row_wistlist").style.height="200px";
            for(i=0; i<data.length;i++){
                let name= data[i].name;
                let id= data[i].id;
                let price= data[i].price;
                let image= data[i].image;
                let url= data[i].url;
                $("#row_wistlist").append('<div class="row"><div class="col-md-3"><img src="'+image+'"></div><div class="col-md-3"><span>'+name+'</span></div><div class="col-md-3"><span>'+price+'VND</span></div><div class="col-md-3"><a href="'+url+'">xem</a><a id="'+id+'"onclick="delete_item(this.id)" href="#" class="delete" style="color:red; margin-left:5px">xóa</a> </div></div>')
                
            }
            $("#number-wistlist").text(data.length)


        }
    }
    view();
    function add_wistlist(checked_id){
        let id= checked_id;
        let name=$("#wistlist_name_"+id).val();
        let image=$("#wistlist_image_"+id).val();
        let price= $('#wistlist_price_'+id).val();
        let url=$("#wistlist_url_"+id).val();
        
        let newItem={
            'url':url,
            'id':id,
            'image':image,
            'price':price,
            'name':name
        }
        if(localStorage.getItem('data')===null){
            localStorage.setItem('data','[]');
        }
        let old_data= JSON.parse(localStorage.getItem('data'));
        
        
        let matches=$.grep(old_data, function(obj){
            return obj.id == id;
        })
        if(matches.length){
            alert("đã tồn tại sản phẩm này");
        }else{
            old_data.push(newItem);
            alert("thêm sản phẩm yêu thích thành công")
            $("#row_wistlist").append('<div class="row"><div class="col-md-3"><img src="'+newItem.image+'"></div><div class="col-md-3"><span>'+newItem.name+'</span></div><div class="col-md-3"><span>'+newItem.price+'</span></div><div class="col-md-3"><a href="'+newItem.url+'">xem</a><a id="'+newItem.id+'"onclick="delete_item(this.id)" href="#" class="delete" style="color:red; margin-left:5px">xóa</a> </div></div>')

        }
        localStorage.setItem('data',JSON.stringify(old_data));

    }
    function delete_item (checked_id){
        let id= checked_id;
        let data= JSON.parse(localStorage.getItem('data'));
        for(i=0;i<data.length;i++){
            if(data[i].id == id){
                data.splice(i,1);
            }
        }
        data=JSON.stringify(data);
        localStorage.setItem('data',data);
        
        alert("xóa thành công");
        
    }
    $(document).on('click','.delete',function(e){
        e.preventDefault();
        $(this).parent().parent().remove();
        
    })
    
</script>