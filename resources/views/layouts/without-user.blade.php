<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/color-purple.css') }}">
    <link rel="stylesheet" href="{{ asset('css/color.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icomoon.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('css/transitions.css') }}">
    <link rel="stylesheet" href="{{ asset('css/cart.css') }}">
    <link href="//fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,700,700i" rel="stylesheet">



    <script src="{{ asset('js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
</head>

<body class="tg-home tg-homeone">
    <div id="tg-wrapper" class="tg-wrapper tg-haslayout">
        <header id="tg-header" class="tg-header tg-haslayout">
            <div class="tg-topbar">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <ul class="tg-addnav">
                                <li>
                                    <a href="javascript:void(0);">
                                        <i class="icon-envelope"></i>
                                        <em>Liên hệ</em>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <i class="icon-question-circle"></i>
                                        <em>Trợ giúp</em>
                                    </a>
                                </li>
                            </ul>
                            <div class=" tg-themedropdown tg-currencydropdown">
                                <a href="/admin-login">
                                    <i class="fas fa-user-tie"></i>
                                    <span>Đăng nhập với tư cách quản lý</span>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tg-middlecontainer">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <strong class="tg-logo"><a href="index-2.html"><img src="images/logo.png"
                                        alt="company name here"></a></strong>
                            <div class="tg-wishlistandcart">
                                <div class="dropdown tg-themedropdown tg-wishlistdropdown">
                                    <a href="javascript:void(0);" id="tg-wishlisst" class="tg-btnthemedropdown"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="tg-themebadge">3</span>
                                        <i class="icon-heart"></i>
                                        <span>Yêu thích</span>
                                    </a>
                                    <div class="dropdown-menu tg-themedropdownmenu" aria-labelledby="tg-wishlisst">
                                        <div class="tg-description">
                                            <p>Không có sản phẩm nào được thêm vào yêu thích!</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="dropdown tg-themedropdown tg-minicartdropdown">
                                    <a href="javascript:void(0);" id="tg-minicart" class="tg-btnthemedropdown"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="tg-themebadge">3</span>
                                        <i class="icon-cart"></i>
                                        <span>$123.00</span>
                                    </a>
                                    <div class="dropdown-menu tg-themedropdownmenu" aria-labelledby="tg-minicart">
                                        <div class="tg-minicartbody">
                                            <div class="tg-minicarproduct">
                                                <figure>
                                                    <img src="images/products/img-01.jpg" alt="image description">

                                                </figure>
                                                <div class="tg-minicarproductdata">
                                                    <h5><a href="javascript:void(0);">Our State Fair Is A Great
                                                            Function</a></h5>
                                                    <h6><a href="javascript:void(0);">$ 12.15</a></h6>
                                                </div>
                                            </div>
                                            <div class="tg-minicarproduct">
                                                <figure>
                                                    <img src="images/products/img-02.jpg" alt="image description">

                                                </figure>
                                                <div class="tg-minicarproductdata">
                                                    <h5><a href="javascript:void(0);">Bring Me To Light</a></h5>
                                                    <h6><a href="javascript:void(0);">$ 12.15</a></h6>
                                                </div>
                                            </div>
                                            <div class="tg-minicarproduct">
                                                <figure>
                                                    <img src="images/products/img-03.jpg" alt="image description">

                                                </figure>
                                                <div class="tg-minicarproductdata">
                                                    <h5><a href="javascript:void(0);">Have Faith In Your Soul</a></h5>
                                                    <h6><a href="javascript:void(0);">$ 12.15</a></h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tg-minicartfoot">
                                            <a class="tg-btnemptycart" href="javascript:void(0);">
                                                <i class="fa fa-trash-o"></i>
                                                <span>Xóa giỏ hàng</span>
                                            </a>
                                            <span class="tg-subtotal">Tổng: <strong>35.78</strong></span>
                                            <div class="tg-btns">
                                                <a class="tg-btn tg-active" href="javascript:void(0);">Xem giỏ hàng</a>
                                                <a class="tg-btn" href="javascript:void(0);">Thanh toán</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tg-searchbox">
                                <form class="tg-formtheme tg-formsearch">
                                    <fieldset>
                                        <input type="text" name="search" class="typeahead form-control"
                                            placeholder="Tìm theo tiêu đề, tên sách, tác giả...">
                                        <button type="submit"><i class="icon-magnifier"></i></button>
                                    </fieldset>
                                    <a href="javascript:void(0);">+ Tìm kiếm nâng cao</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </header>
        @yield('cart')
        @yield('checkout')
        @yield('user')
    </div>


    <script src="{{ asset('js/vendor/jquery-library.js') }}"></script>
    <script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyCR-KEWAVCn52mSdeVeTqZjtqbmVJyfSus&amp;language=en"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/jquery.vide.min.js') }}"></script>
    <script src="{{ asset('js/countdown.js') }}"></script>
    <script src="{{ asset('js/jquery-ui.js') }}"></script>
    <script src="{{ asset('js/parallax.js') }}"></script>
    <script src="{{ asset('js/countTo.js') }}"></script>
    <script src="{{ asset('js/appear.js') }}"></script>
    <script src="{{ asset('js/gmap3.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
</body>

</html>
