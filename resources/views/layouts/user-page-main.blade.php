<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Trang bán sách</title>
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/color-purple.css') }}">
    <link rel="stylesheet" href="{{ asset('css/color.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icomoon.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('css/transitions.css') }}">
    <link rel="stylesheet" href="{{ asset('css/cart.css') }}">
    <link href="//fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css"
        integrity="sha512-Fo3rlrZj/k7ujTnHg4CGR2D7kSs0v4LLanw2qksYuRlEzO+tcaEPQogQ0KaoGN26/zrn20ImR1DfuLWnOo7aBA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="{{ asset('js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
</head>

<body class="tg-home tg-homeone">
    <div id="tg-wrapper" class="tg-wrapper tg-haslayout">
        <header id="tg-header" class="tg-header tg-haslayout">
            <div class="tg-topbar">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <ul class="tg-addnav">
                                <li>
                                    <a href="javascript:void(0);">
                                        <i class="icon-envelope"></i>
                                        <em>Liên hệ</em>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <i class="icon-question-circle"></i>
                                        <em>Trợ giúp</em>
                                    </a>
                                </li>
                            </ul>
                            <div class=" tg-themedropdown tg-currencydropdown">
                                <a href="{{ route('admin-login') }}">
                                    <i class="fas fa-user-tie"></i>
                                    <span>Đăng nhập với tư cách quản lý</span>
                                </a>

                            </div>
                            <div class="tg-userlogin">
                                <div class="dropdown tg-themedropdown tg-currencydropdown">
                                    <?php $user = session()->get('user'); ?>
                                    <a href="{{ $user == null ? '/login' : '' }}" id="tg-currenty"
                                        class="tg-btnthemedropdown"
                                        data-toggle="{{ $user !== null ? 'dropdown' : '' }}" aria-haspopup="true"
                                        aria-expanded="false">
                                        <span>

                                            {{ $user !== null ? $user['username'] : 'Đăng nhập' }}
                                        </span>
                                    </a>
                                    <ul class="dropdown-menu tg-themedropdownmenu" aria-labelledby="tg-currenty">
                                        <li>
                                            <a href="/logout">

                                                <span>Đăng xuất</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/change-pass">
                                                <span>Đổi mật khẩu</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tg-middlecontainer">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <strong class="tg-logo"><a href="{{ route('home') }}"><img
                                        src="{{ asset('images/logo.png') }}" alt="company name here"></a></strong>
                            <div class="tg-wishlistandcart">
                                <div class="dropdown tg-themedropdown tg-wishlistdropdown">
                                    <a href="javascript:void(0);" id="tg-wishlisst" class="tg-btnthemedropdown"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="tg-themebadge" id="number-wistlist"></span>
                                        <i class="icon-heart"></i>
                                        <span>Yêu thích</span>
                                    </a>
                                    <div class="dropdown-menu tg-themedropdownmenu" id="row_wistlist">


                                        {{-- <div class="tg-description"><p>Không có sản phẩm nào được thêm vào yêu thích!</p></div> --}}
                                    </div>
                                </div>
                                <div class="dropdown tg-themedropdown tg-minicartdropdown">
                                    <a href="/showCart" id="tg-minicart" class="tg-btnthemedropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                        <span
                                            class="tg-themebadge">{{ session()->get('cart') !== null ? count(session()->get('cart')) : 0 }}</span>
                                        <i class="icon-cart"></i>
                                        <span>Giỏ hàng</span>
                                    </a>
                                </div>
                            </div>
                            <div class="tg-searchbox">
                                <form class="tg-formtheme tg-formsearch">
                                    <fieldset>
                                        <input type="text" name="search" id="search" class="form-control"
                                            placeholder="Tìm theo tiêu đề, tên sách, tác giả...">
                                        <button type="submit"><i class="icon-magnifier"></i></button>
                                        <table id='search-result' class='search-result'>

                                        </table>
                                    </fieldset>

                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="tg-navigationarea">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <nav id="tg-nav" class="tg-nav">
                                <div class="navbar-header">

                                </div>
                                <div id="tg-navigation" class="collapse navbar-collapse tg-navigation">
                                    <ul style="display: flex; justify-content:space-between">
                                        <li class="menu-item current-menu-item">
                                            <a href="{{ route('home') }}">Trang chủ</a>
                                        </li>
                                        <li class="menu-item-has-children menu-item">
                                            <a href="">Tất cả danh mục</a>
                                            <div class="mega-menu">
                                                <ul class="tg-themetabnav" role="tablist">
                                                    @foreach ($categories as $category)
                                                        <li role="presentation" class="active">
                                                            <a
                                                                href="/category_detail/{{ $category->id }}">{{ $category->category_name }}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </li>

                                        <li class="menu-item">
                                            <a href="/authors">Tác giả</a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="/books">Sách</a>
                                        </li>
                                        <li class="menu-item-has-children">
                                            <a href="javascript:void(0);">Tin mới nhất</a>
                                            <ul class="sub-menu">
                                                <li><a href="javascript:void(0);">Danh sách tin</a></li>

                                                <li><a href="javascript:void(0);">Chi tiết tin</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="javascript:void(0);">Liên hệ</a></li>
                                        <li class="menu-item-has-children current-menu-item">
                                            <a href="javascript:void(0);"><i class="icon-menu"></i></a>
                                            <ul class="sub-menu">
                                                <li class="menu-item-has-children">
                                                    <a href="javascript:void(0);">Sản phẩm</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="javascript:void(0);">Sản phẩm</a></li>
                                                        <li><a href="javascript:void(0);">Chi tiết sản phẩm</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="javascript:void(0);">Giới thiệu</a></li>
                                                <li><a href="javascript:void(0);">Sắp ra mắt</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>


        @yield('cart')
        @yield('checkout')
        @yield('user')
        @yield('index1')
        @yield('authors')
        @yield('authors_detail')
        @yield('books')
        @yield('books_detail')
        {{-- @include('user-layouts.navbar-menu') --}}
        @yield('content')

        <footer id="tg-footer" class="tg-footer tg-haslayout">
            <div class="tg-footerarea">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <ul class="tg-clientservices">
                                <li class="tg-devlivery">
                                    <span class="tg-clientserviceicon"><i class="icon-rocket"></i></span>
                                    <div class="tg-titlesubtitle">
                                        <h3>Chuyển phát</h3>
                                    </div>
                                </li>
                                <li class="tg-discount">
                                    <span class="tg-clientserviceicon"><i class="icon-tag"></i></span>
                                    <div class="tg-titlesubtitle">
                                        <h3>Giảm giá</h3>
                                    </div>
                                </li>
                                <li class="tg-quality">
                                    <span class="tg-clientserviceicon"><i class="icon-leaf"></i></span>
                                    <div class="tg-titlesubtitle">
                                        <h3>Chất lượng</h3>
                                    </div>
                                </li>
                                <li class="tg-support">
                                    <span class="tg-clientserviceicon"><i class="icon-heart"></i></span>
                                    <div class="tg-titlesubtitle">
                                        <h3>Hỗ trợ 24/7</h3>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="tg-threecolumns">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="tg-footercol">
                                    <strong class="tg-logo"><a href="javascript:void(0);"><img
                                                src="{{ asset('images/flogo.png') }}"
                                                alt="image description"></a></strong>
                                    <ul class="tg-contactinfo">
                                        <li>
                                            <i class="icon-apartment"></i>
                                            <address>51 Lê Đại Hành</address>
                                        </li>
                                        <li>
                                            <i class="icon-phone-handset"></i>
                                            <span>
                                                <em>0385508579</em>
                                                <em>0210868686</em>
                                            </span>
                                        </li>
                                        <li>
                                            <i class="icon-clock"></i>
                                            <span>Phục vụ 24/7</span>
                                        </li>
                                        <li>
                                            <i class="icon-envelope"></i>
                                            <span>
                                                <em><a href="mailto:support@domain.com">support@domain.com</a></em>
                                                <em><a href="mailto:info@domain.com">info@domain.com</a></em>
                                            </span>
                                        </li>
                                    </ul>
                                    <ul class="tg-socialicons">
                                        <li class="tg-facebook"><a href="javascript:void(0);"><i
                                                    class="fab fa-facebook"></i></a></li>
                                        <li class="tg-twitter"><a href="javascript:void(0);"><i
                                                    class="fab fa-twitter"></i></a></li>
                                        <li class="tg-linkedin"><a href="javascript:void(0);"><i
                                                    class="fab fa-linkedin"></i></a></li>
                                        <li class="tg-googleplus"><a href="javascript:void(0);"><i
                                                    class="fab fa-google"></i></a></li>
                                        <li class="tg-rss"><a href="javascript:void(0);"><i
                                                    class="fa fa-rss"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="tg-footercol tg-widget tg-widgetnavigation">
                                    <div class="tg-widgettitle">
                                        <h3>Thông tin liên quan</h3>
                                    </div>
                                    <div class="tg-widgetcontent d-flex">
                                        <ul>
                                            <li><a href="javascript:void(0);">Điều khoản sử dụng</a></li>
                                            <li><a href="javascript:void(0);">Điều khoản mua bán</a></li>
                                            <li><a href="javascript:void(0);">Quyền riêng tư</a></li>
                                            <li><a href="javascript:void(0);">Cookies</a></li>
                                        </ul>
                                        <ul>
                                            <li><a href="javascript:void(0);">Liên hệ</a></li>
                                            <li><a href="javascript:void(0);">Chi nhánh </a></li>
                                            <li><a href="javascript:void(0);">Tầm nhìn &amp; Mục tiêu</a></li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tg-newsletter">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <h4>Đăng kí nhận tin mới nhất</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <form class="tg-formtheme tg-formnewsletter">
                                <fieldset>
                                    <input type="email" name="email" class="form-control"
                                        placeholder="Nhập email của bạn">
                                    <button type="button"><i class="icon-envelope"></i></button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tg-footerbar">
                <a id="tg-btnbacktotop" class="tg-btnbacktotop" href="javascript:void(0);"><i
                        class="icon-chevron-up"></i></a>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <span class="tg-copyright">2021 All Rights Reserved By &copy; N1 Team</span>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    @include('layouts.books.add_to_wistlist')
    @include('search')

    <script src="{{ asset('js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery-library.js') }}"></script>
    <script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyCR-KEWAVCn52mSdeVeTqZjtqbmVJyfSus&amp;language=en"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/jquery.vide.min.js') }}"></script>
    <script src="{{ asset('js/countdown.js') }}"></script>
    <script src="{{ asset('js/jquery-ui.js') }}"></script>
    <script src="{{ asset('js/parallax.js') }}"></script>
    <script src="{{ asset('js/countTo.js') }}"></script>
    <script src="{{ asset('js/appear.js') }}"></script>
    <script src="{{ asset('js/gmap3.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <!-- Messenger Plugin chat Code -->
    <div id="fb-root"></div>

    <!-- Your Plugin chat code -->
    <div id="fb-customer-chat" class="fb-customerchat">
    </div>

    <script>
        var chatbox = document.getElementById('fb-customer-chat');
        chatbox.setAttribute("page_id", "101497179102474");
        chatbox.setAttribute("attribution", "biz_inbox");
    </script>

    <!-- Your SDK code -->
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                xfbml: true,
                version: 'v13.0'
            });
        };

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
</body>

</html>
