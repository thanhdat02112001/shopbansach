@extends('layouts.admin-page-main')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Thông tin cá nhân</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Thông tin cá nhân</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">

                        <!-- Profile Image -->
                        <div class="card card-primary card-outline">
                            <div class="card-body box-profile">
                                <div class="text-center">
                                    <img class="profile-user-img img-fluid img-circle fas fa-user-circle"
                                        src="{{ Storage::disk('admin_avatar')->url($admin->avatar) }}" alt="">
                                </div>

                                <h3 class="profile-username text-center">{{ $admin->name }}</h3>

                                <p class="text-muted text-center">Quản trị viên</p>

                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                        <b>Người theo dõi</b> <a class="float-right">1,322</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Đang theo dõi</b> <a class="float-right">543</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Bạn bè</b> <a class="float-right">13,287</a>
                                    </li>
                                </ul>

                                <a href="{{ route('admin-login') }}" class="btn btn-primary btn-block"><b>Đăng xuất</b></a>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                        <!-- About Me Box -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Thông tin</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <strong><i class="fas fa-book mr-1"></i> Mạng xã hội</strong>

                                <p class="text-muted">
                                    {{ $admin->social_network }}
                                </p>

                                <hr>

                                <strong><i class="fas fa-map-marker-alt mr-1"></i> Địa chỉ</strong>

                                <p class="text-muted">{{ $admin->address }}</p>

                                <hr>

                                <strong><i class="fas fa-phone mr-1"></i> Phone</strong>

                                <p class="text-muted">
                                    {{ $admin->phone }}
                                </p>

                                <hr>

                                <strong><i class="fas fa-envelope mr-1"></i> Gmail</strong>

                                <p class="text-muted">{{ $admin->email }}</p>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header p-2">
                                <button type="button" class="btn btn-primary" href="#settings" data-toggle="tab">Cập nhật
                                    thông tin cá nhân</button>

                            </div><!-- /.card-header -->
                            {{-- <div class="card-body"> --}}
                            <div class="tab-content">
                                <div class="tab-pane" id="settings">
                                    <form action="page-profile" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row" style="padding: 5px 15px; margin: 0px">
                                            <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="name" class="form-control" id="inputName"
                                                    placeholder="Họ và tên">
                                            </div>
                                            @error('name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group row" style="padding: 5px 15px; margin: 0px">
                                            <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                                            <div class="col-sm-10">
                                                <input type="email" name="email" class="form-control" id="inputEmail"
                                                    placeholder="Email">
                                            </div>
                                            @error('email')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group row" style="padding: 5px 15px; margin: 0px">
                                            <label for="inputName2" class="col-sm-2 col-form-label">Phone</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="phone" class="form-control" id="inputName2"
                                                    placeholder="Số điện thoại">
                                            </div>
                                            @error('phone')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group row" style="padding: 5px 15px; margin: 0px">
                                            <label for="inputsocial" class="col-sm-2 col-form-label">Social Network</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="social_network" class="form-control"
                                                    id="inputsocial" placeholder="Mạng xã hội">
                                            </div>
                                            @error('social_network')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group row" style="padding: 5px 15px; margin: 0px">
                                            <label for="inputExperience" class="col-sm-2 col-form-label">Address</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="address" class="form-control"
                                                    id="inputExperience" placeholder="Địa chỉ">
                                            </div>
                                            @error('address')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group row" style="padding: 5px 15px; margin: 0px">
                                            <label for="inputSkills" class="col-sm-2 col-form-label">Date of birth</label>
                                            <div class="col-sm-10">
                                                <input type="date" name="dob" class="form-control" id="inputSkills"
                                                    placeholder="Ngày sinh">
                                            </div>
                                            @error('dob')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group row" style="padding: 5px 15px; margin: 0px">
                                            <label for="avatar" class="col-sm-2 col-form-label">Avatar</label>
                                            <div class="col-sm-10">
                                                <input type="file" name="avatar" class="form-control" id="avatar"
                                                    placeholder="Ảnh đại diện" style="height: 100%">
                                            </div>
                                            @error('avatar')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group row" style="padding: 5px">
                                            <div class="offset-sm-2 col-sm-10">
                                                <button type="submit" class="btn btn-danger">Thay đổi</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                            {{-- </div><!-- /.card-body --> --}}
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
