@extends('layouts.admin-page-main')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Sửa tác giả</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Sửa tác giả</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <form action="edit" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="card card-outline card-info">
                            <div class="card-header">
                                <h3 class="card-title">
                                    Sửa tác giả
                                </h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="author_name">Tên tác giả</label>
                                    <input class="form-control" type="text" name="author_name" id="author_name"
                                        placeholder="Author Name" value="{{ $author->author_name }}">
                                    @error('author_name')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="author_products">Tác phẩm nổi bật</label>
                                    <input class="form-control" type="text" name="author_products" id="author_products"
                                        placeholder="Featured Book" value="{{ $author->author_products }}">
                                    @error('author_products')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="author_biography">Tiểu sử</label>
                                    <textarea class="form-control" name="author_biography" id="author_biography" cols="30" rows="10"
                                        placeholder="Description">{{ $author->author_biography }}</textarea>
                                    @error('author_biography')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="author_avatar">Chọn ảnh tác giả</label>
                                    <input class="form-control" type="file" name="author_avatar" id="author_avatar"
                                        style="height: 100%">
                                    @error('author_avatar')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Cập nhật tác giả</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
