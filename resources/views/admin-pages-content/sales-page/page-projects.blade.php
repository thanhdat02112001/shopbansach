@extends('layouts.admin-page-main')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Tình trạng đơn hàng</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Tình trạng</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Tình trạng giao hàng</h3>
                    <div class="card-tools">
                        <form action="{{ route('page-projects') }}" method="GET">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="search" name="order_id_search" class="form-control float-right"
                                    placeholder="Search">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card-body p-0" style="text-align: center">
                    <table class="table table-striped projects">
                        <thead>
                            <tr>
                                <th>
                                    STT
                                </th>
                                <th>
                                    Order ID
                                </th>
                                <th>
                                    Tên và địa chỉ khách hàng
                                </th>
                                <th>
                                    Ngày đặt hàng
                                </th>
                                <th>
                                    Shop đang chuẩn bị hàng
                                </th>
                                <th>
                                    Đã giao cho bên vận chuyển
                                </th>
                                <th>
                                    Đang giao hàng
                                </th>
                                <th>
                                    Đã giao
                                </th>
                                <th>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders as $order)
                                <tr>
                                    <td>
                                        {{ $order->id }}
                                    </td>
                                    <td>
                                        #{{ $order->created_at->format('Ymd') }}{{ $order->id }}
                                    </td>
                                    <td>
                                        <strong>{{ $order->transaction->user->name }}</strong><br>
                                        {{ $order->transaction->transaction_address }}<br>
                                        Phone: {{ $order->transaction->transaction_phone }}<br>
                                        Email: {{ $order->transaction->transaction_email }}
                                    </td>
                                    <td>
                                        {{ $order->created_at }}
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td><i class="fas fa-check"></i></td>
                                    <td></td>
                                    <td class="project-actions text-right btn-message">
                                        <div
                                            style="color: blue; display: {{ $order->order_status == 2 ? '' : 'none' }}">
                                            <p>Đã hoàn thành</p>
                                        </div>
                                        <div
                                            style="color: brown; display: {{ $order->order_status == 3 ? '' : 'none' }}">
                                            <p>Đơn hàng hủy</p>
                                        </div>
                                        <div style="display: {{ $order->order_status == 1 ? '' : 'none' }}">
                                            <a class="btn btn-primary btn-sm btn-order-success-status"
                                                data-order-id="{{ $order->id }}">
                                                <i class="fas fa-folder">
                                                </i>
                                                Hoàn thành
                                            </a>

                                            <a class="btn btn-danger btn-sm btn-order-cancel-status"
                                                data-order-id="{{ $order->id }}">
                                                <i class="fas fa-trash">
                                                </i>
                                                Hủy đơn hàng
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <div class="row">
                <div class="col-12">
                    {{ $orders->links() }}
                </div>
            </div>
            <!-- /.card -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection