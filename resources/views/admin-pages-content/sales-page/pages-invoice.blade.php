@extends('layouts.admin-page-main')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Đơn hàng mới</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Đơn hàng mới</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        @foreach ($orders as $order)
                            <tr>
                                <!-- Main content -->
                                <div class="invoice p-3 mb-3">
                                    <!-- title row -->
                                    <div class="row">
                                        <div class="col-12">
                                            <h4>
                                                <img src="{{ asset('images/logo.png') }}" />
                                                <small class="float-right">Ngày đặt hàng:
                                                    {{ $order->created_at }}</small>
                                            </h4>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- info row -->
                                    <div class="row invoice-info">
                                        <div class="col-sm-5 invoice-col">
                                            <i>Địa chỉ cửa hàng</i>
                                            <address>
                                                <strong>Book Library</strong><br>
                                                Quận Hai Bà Trưng, Hà Nội<br>
                                                Phone: (0777) 142-604<br>
                                                Email: booklibrary-haibatrung@gmail.com
                                            </address>
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-sm-5 invoice-col">
                                            <i>Địa chỉ khách hàng</i>
                                            <address>
                                                {{-- @foreach ($order->transaction as $transaction) --}}
                                                <strong>{{ $order->transaction->user->name }}</strong><br>
                                                {{ $order->transaction->transaction_address }}<br>
                                                Phone: {{ $order->transaction->transaction_phone }}<br>
                                                Email: {{ $order->transaction->transaction_email }}
                                                {{-- @endforeach --}}
                                            </address>
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-sm-2 invoice-col">
                                            <br>
                                            <b>Order ID:</b>
                                            #{{ $order->created_at->format('Ymd') }}{{ $order->id }}<br>

                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                    <!-- Table row -->
                                    <div class="row">
                                        <div class="col-12 table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Tên sản phẩm</th>

                                                        <th style="text-align: right;">Giá</th>
                                                        <th style="text-align: right;">Số lượng</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($order->order_detail as $item)
                                                        <tr>
                                                            <td>{{ $item->product_name }}</td>
                                                            <td style="text-align: right;">
                                                                {{ number_format($item->product_price) }}</td>
                                                            <td style="text-align: right;">{{ $item->product_quantity }}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                    <div class="row">
                                        <!-- accepted payments column -->
                                        <div class="col-4">
                                            <p class="lead">Hình thức thanh toán:</p>
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <tr>
                                                        <th>Thanh toán bằng thẻ</th>
                                                        <td><i
                                                                class="{{ $order->payment_type == 2 ? 'fas fa-check' : '' }}"></i>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Thanh toán tiền mặt</th>
                                                        <td><i
                                                                class="{{ $order->payment_type == 1 ? 'fas fa-check' : '' }}"></i>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-8">
                                            <p class="lead">Tổng thanh toán</p>
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <tr>
                                                        <th style="width:50%">Tổng giá:</th>
                                                        <td>{{ number_format($order->total_money) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Phí ship:</th>
                                                        <td>{{ number_format(50000) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Tổng thanh toán:</th>
                                                        <td>{{ number_format($order->total_money + 50000) }}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                    <!-- this row will not appear when printing -->
                                    <div class="row no-print">
                                        <div class="col-12">
                                            <a href="invoice-print.html" rel="noopener" target="_blank"
                                                class="btn btn-default"><i class="fas fa-print"></i> Print</a>
                                            <button data-order-id="{{ $order->id }}" type="button"
                                                class="btn btn-success float-right btn-order-accept-status">
                                                <i class="far fa-credit-card"></i>
                                                Xác nhận
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </tr>
                        @endforeach
                        <!-- /.invoice -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-12">
                        {{ $orders->links() }}
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection