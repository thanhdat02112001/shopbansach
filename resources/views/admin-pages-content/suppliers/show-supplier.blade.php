@extends('layouts.admin-page-main')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Xem chi tiết nhà xuất bản </h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Xem chi tiết</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Nhà xuất bản:<i style="color:brown">
                                        {{ $supplier->supplier_name }}</i></h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0" style="height: auto;">
                                <table class="table table-head-fixed text-nowrap" style="text-align: center">
                                    <thead>
                                        <tr>
                                            <th>ID Sách</th>
                                            <th>Tên sách</th>
                                            <th>Tên tác giả</th>
                                            <th>Thể loại</th>
                                            <th>Số lượng</th>
                                            <th>Đã bán</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($supplier->products as $product)
                                            <tr>
                                                <td>{{ $product->id }}</td>
                                                <td>{{ $product->product_name }}</td>
                                                <td>{{ $product->author->author_name }}</td>
                                                <td>{{ $product->category->category_name }}</td>
                                                <td>{{ $product->product_quantity }}</td>
                                                <td>{{ $product->product_sale }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->
                <div class="row" style="padding-top: 50px">
                    <div class="col-md-12">
                        <div>
                            <a class="btn btn-primary btn-sm" href="{{ route('list-supplier') }}">
                                <i class="fas fa-arrow-left"></i>
                                </i>
                                Quay lại
                            </a>
                        </div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection