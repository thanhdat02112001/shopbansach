@extends('layouts.admin-page-main')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Sửa nhà xuất bản</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Sửa nhà xuất bản</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <form action="edit" method="POST">
                        @csrf
                        <div class="card card-outline card-info">
                            <div class="card-header">
                                <h3 class="card-title">
                                    Sửa
                                </h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="supplier_name">Tên nhà xuất bản</label>
                                    <input class="form-control" type="text" name="supplier_name" id="supplier_name"
                                        placeholder="Supplier Name" value="{{ $supplier->supplier_name }}">
                                    @error('supplier_name')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="supplier_phone">Số điện thoại</label>
                                    <input class="form-control" type="text" name="supplier_phone" id="supplier_phone"
                                        placeholder="Phone Number" value="{{ $supplier->supplier_phone }}">
                                    @error('supplier_phone')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="supplier_email">Email</label>
                                    <input class="form-control" type="email" name="supplier_email" id="supplier_email"
                                        placeholder="Email" value="{{ $supplier->supplier_email }}">
                                    @error('supplier_email')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="supplier_address">Địa chỉ</label>
                                    <textarea class="form-control" name="supplier_address" id="supplier_address" cols="30" rows="10"
                                        placeholder="Address">{{ $supplier->supplier_address }}</textarea>
                                    @error('supplier_address')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Cập nhật nhà xuất bản</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection