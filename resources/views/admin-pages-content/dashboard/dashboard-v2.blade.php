@extends('layouts.admin-page-main')


@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Thống kê đơn hàng</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Thống kê đơn hàng</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Info boxes -->
                <div class="row">

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-success elevation-1"><i class="fas fa-cart-plus"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Đơn hàng mới</span>
                                <span class="info-box-number">{{ count($newOrder) }}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="far fa-calendar-check"></i></span>


                            <div class="info-box-content">
                                <span class="info-box-text">Đơn hàng thành công</span>
                                <span class="info-box-number">{{ count($successOrder) }}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-danger elevation-1"><i class="far fa-calendar-times"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Đơn hàng hủy</span>
                                <span class="info-box-number">{{ count($cancelOrder) }}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->

                    <!-- fix for small devices only -->
                    <div class="clearfix hidden-md-up"></div>

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-cart-arrow-down"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Tổng đơn hàng</span>
                                <span class="info-box-number">{{ $totalOrder }}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <canvas id="myChart"></canvas>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.0/chart.min.js"
        integrity="sha512-TW5s0IT/IppJtu76UbysrBH9Hy/5X41OTAbQuffZFU6lQ1rdcLHzpU5BzVvr/YFykoiMYZVWlr/PX1mDcfM9Qg=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        const ctx = document.getElementById('myChart').getContext('2d');
        const data = <?= json_encode($data) ?>;
        const data2 = <?= json_encode($data2) ?>;
        const data3 = <?= json_encode($data3) ?>;
        const myChart = new Chart(ctx, {
            data: {
                labels: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8',
                    'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'
                ],
                datasets: [{
                        label: 'Số đơn hàng thành công',
                        data: data2,
                        type: 'line',
                        fill: false,
                        backgroundColor: 'rgba(3, 138, 225, 1)',
                        borderColor: 'rgba(0, 181, 204, 1)',
                        borderWidth: 0.5
                    },
                    {
                        label: 'Số đơn hàng đã hủy',
                        data: data3,
                        type: 'line',
                        fill: false,
                        backgroundColor: 'rgba(150, 40, 27, 1)',
                        borderColor: 'rgba(236, 100, 75, 1)',
                        borderWidth: 0.5
                    },
                    {
                        label: 'Tổng số đơn hàng của tháng',
                        data: data,
                        type: 'bar',
                        backgroundColor: 'rgba(245, 230, 83, 0.6)',
                        borderColor: 'rgba(255, 240, 0, 1)',
                        borderWidth: 1,
                        minBarLength: 1
                    }

                ]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    </script>
@endsection
