@extends('layouts.admin-page-main')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Thêm sách mới</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Thêm sách mới</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="card card-outline card-info">
                            <div class="card-header">
                                <h3 class="card-title">Thêm mới</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form action="create-product" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="supplier">Nhà xuất bản</label>
                                        <select name="product_supplier_id" id="supplier" class="form-control">
                                            @foreach ($suppliers as $supplier)
                                                <option value="{{ $supplier->id }}">{{ $supplier->supplier_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('product_supplier_id')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="category">Thể loại</label>
                                        <select name="product_category_id" id="category" class=" form-control">
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->category_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('product_category_id')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="pro_name">Tên sách</label>
                                        <input type="text" class="form-control" name="product_name" id="pro_name"
                                            placeholder="Book Name" value="{{ old('product_name') }}">
                                        @error('product_name')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="pro_slug">Đường dẫn URL</label>
                                        <input type="text" class="form-control" name="product_slug" id="pro_slug"
                                            placeholder="URL" value="{{ old('product_slug') }}">
                                        @error('product_slug')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="author">Tên tác giả</label>
                                        <select name="product_author_id" id="author" class=" form-control">
                                            @foreach ($authors as $author)
                                                <option value="{{ $author->id }}">{{ $author->author_name }}</option>
                                            @endforeach
                                        </select>
                                        @error('product_author_id')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Mô tả</label>
                                        <textarea class="form-control" name="product_description" id="description" cols="30" rows="10"
                                            placeholder="Description">{{ old('product_description') }}</textarea>
                                        @error('product_description')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="content">Tóm tắt nội dung</label>
                                        <textarea class="form-control" name="product_content" id="content" cols="30" rows="10"
                                            placeholder="Introduce">{{ old('product_content') }}</textarea>
                                        @error('product_content')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="quantity">Số lượng</label>
                                        <input type="text" class="form-control" name="product_quantity" id="quantity"
                                            placeholder="Quantity" value="{{ old('product_quantity') }}">
                                        @error('product_quantity')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="price">Giá sách</label>
                                        <input type="text" class="form-control" name="product_price" id="price"
                                            placeholder="Book Price" value="{{ old('product_price') }}">
                                        @error('product_price')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputFile">Chọn ảnh</label>
                                        <input type="file" class="form-control" name="product_avatar[]"
                                            id="exampleInputFile" style="height: 100%">
                                        @error('product_avatar')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <!-- /.card-body -->

                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary">Lưu sản phẩm</button>
                                    </div>
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (left) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
