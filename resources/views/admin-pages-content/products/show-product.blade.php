@extends('layouts.admin-page-main')

@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="container-fluid" style="padding: 50px">
                <div class="row">
                    <div class="col-md-5" style="text-align: center">
                        <img src="{{ Storage::disk('product_avatar')->url($product->product_avatar) }}"
                            alt="Book Picture" style="width: 80%; border-radius: 30px">
                    </div>
                    <div class="col-md-7">
                        <div class="row">
                            <div class="col-md-12">
                                <h2><i>"{{ $product->product_name }}"</i></h2>
                                <div style="padding-left: 30px">
                                    <p><i>Tác giả:</i><b style="padding-left: 10px">
                                            {{ $product->author->author_name }}</b> </p>
                                    <p><i>Thể loại:</i><b style="padding-left: 10px">
                                            {{ $product->category->category_name }}</b></p>
                                    <p><i>Nhà xuất bản:</i><b style="padding-left: 10px">
                                            {{ $product->supplier->supplier_name }}</b></p>
                                    <p><i>Mô tả:</i><b style="padding-left: 10px"> {{ $product->product_description }}</b>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-top: 30px">
                            <div class="col-md-12">
                                <h4><i style="padding-right: 5px; color: brown" class="fas fa-info-circle"></i>Tóm tắt nội
                                    dung tác phẩm</h4>
                                <div style="padding-left: 30px">
                                    <p>{{ $product->product_content }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="padding-top: 50px">
                    <div class="col-md-12">
                        <div>
                            <a class="btn btn-primary btn-sm" href="{{ route('list-product') }}">
                                <i class="fas fa-arrow-left"></i>
                                </i>
                                Quay lại
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
