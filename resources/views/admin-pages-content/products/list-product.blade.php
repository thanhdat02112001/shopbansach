@extends('layouts.admin-page-main')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Danh sách số lượng sách </h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Danh sách</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Tất cả</h3>

                                <div class="card-tools">
                                    <form action="{{ route('list-product') }}" method="GET">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="search" name="product_search" class="form-control float-right"
                                                placeholder="Search">

                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-default">
                                                    <i class="fas fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0" style="height: auto;">
                                <table class="table table-head-fixed text-nowrap" style="text-align: center; ">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Hình ảnh</th>
                                            <th>Tên sách</th>
                                            <th>Ngày cập nhật</th>
                                            <th>Tên tác giả</th>
                                            <th>Số lượng</th>
                                            <th>Đã bán</th>
                                            <th>Hành động</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($products as $product)
                                            <tr>
                                                <td>{{ $product->id }}</td>
                                                <td><img src="{{ Storage::disk('product_avatar')->url($product->product_avatar) }}"
                                                        alt="Book Picture" style="width: 100px; height: 100px"></td>
                                                <td>{{ $product->product_name }}</td>
                                                <td>{{ $product->created_at->format('d-m-Y') }}</td>
                                                <td>{{ $product->author->author_name }}</td>
                                                <td>{{ $product->product_quantity }}</td>
                                                <td>{{ $product->product_sale }}</td>
                                                <td>
                                                    <a class="btn btn-primary btn-sm"
                                                        href="product/{{ $product->product_slug }}">
                                                        <i class="fas fa-folder">
                                                        </i>
                                                        Xem
                                                    </a>
                                                    <a class="btn btn-info btn-sm"
                                                        href="product/{{ $product->product_slug }}/edit">
                                                        <i class="fas fa-pencil-alt">
                                                        </i>
                                                        Sửa
                                                    </a>
                                                    <a onclick="return confirm('Bạn đồng ý xóa thông tin sản phẩm này!')"
                                                        class="btn btn-danger btn-sm"
                                                        href="product/{{ $product->id }}/delete">
                                                        <i class="fas fa-trash">
                                                        </i>
                                                        Xóa
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{-- <span>
                    {{ $product->chuck() }}

                  </span> --}}
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-12">
                        {{ $products->links() }}
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
