@extends('layouts.main')
@section('user')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align: center">
                        <h3 class="panel-title">Đăng nhập quản lý </h3>
                    </div>
                    <div class="panel-body">
                        <form accept-charset="UTF-8" role="form">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Email" name="email" type="text"
                                        style="border: 1px solid black">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Mật khẩu" name="password" type="password"
                                        value="" style="border: 1px solid black">
                                </div>

                                <label>
                                    <input name="remember" type="checkbox" value="Remember Me"> Nhớ mật khẩu
                                </label>

                                <input class="btn btn-lg btn-success btn-block" type="submit" value="Đăng nhập">
                            </fieldset>
                        </form>
                        <a href="/forgot-password">Quên mật khẩu?</a>

                    </div>
                </div>
            </div>
        </div>
</div @endsection
