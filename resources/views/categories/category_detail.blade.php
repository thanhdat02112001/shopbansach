@extends('layouts.user-page-main')
@section('content')
    <div class="tg-innerbanner tg-haslayout tg-parallax tg-bginnerbanner" data-z-index="-100" data-appear-top-offset="600"
        data-parallax="scroll" data-image-src="images/parallax/bgparallax-07.jpg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-innerbannercontent">
                        <h1>{{ $category->category_name }}</h1>
                        <ol class="tg-breadcrumb">
                            <li><a href="{{ route('home') }}">Trang chủ</a></li>
                            <li><a href="#">Danh mục</a></li>
                            <li class="tg-active">{{ $category->category_name }}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <main id="tg-main" class="tg-main tg-haslayout">
        <!--************************************
                Author Detail Start
        *************************************-->
        <div class="tg-sectionspace tg-haslayout">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-authordetail">

                            <div class="tg-authorcontentdetail">
                                <div class="tg-booksfromauthor">
                                    <div class="tg-sectionhead">
                                        <h2><b>Thể loại:</b> <i>{{ $category->category_name }}</i></h2>
                                    </div>
                                    <div class="row">
                                        @foreach ($productCategories as $product)
                                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
                                                <div class="tg-postbook">
                                                    <figure class="tg-featureimg" style="width:200px; height:250px">
                                                        <div class="tg-bookimg">
                                                            <div class="tg-frontcover"><img
                                                                    src="{{ Storage::disk('product_avatar')->url($product->product_avatar) }}"
                                                                    alt="image description"></div>
                                                            <div class="tg-backcover"><img
                                                                    src="{{ Storage::disk('product_avatar')->url($product->product_avatar) }}"
                                                                    alt="image description"></div>
                                                        </div>
                                                        <a class="tg-btnaddtowishlist" href="javascript:void(0);"
                                                            id="{{ $product->id }}" onclick="add_wistlist(this.id)">
                                                            <i class="icon-heart"></i>
                                                            <span>Thêm vào yêu thích</span>
                                                        </a>
                                                    </figure>
                                                    <div class="tg-postbookcontent">

                                                        <div class="tg-themetagbox"><span class="tg-themetag">sale</span>
                                                        </div>
                                                        <div class="tg-booktitle">
                                                            <h3><a
                                                                    href="javascript:void(0);">{{ $product->product_name }}</a>
                                                            </h3>
                                                            <input type="hidden" id="wistlist_name_{{ $product->id }}"
                                                                value="{{ $product->product_name }}">
                                                            <input type="hidden" id="wistlist_url_{{ $product->id }}"
                                                                value="{{ route('product_detail', $product->id) }}">
                                                            <input type="hidden" id="wistlist_image_{{ $product->id }}"
                                                                value="{{ Storage::disk('product_avatar')->url($product->product_avatar) }}">

                                                        </div>
                                                        <span class="tg-bookwriter">Tác giả: <a
                                                                href="/author_detail/{{ $product->author->id }}">{{ $product->author->author_name }}</a></span>
                                                        <span class="tg-stars"><span></span></span>
                                                        <span class="tg-bookprice">
                                                            <input type="hidden" id="wistlist_price_{{ $product->id }}"
                                                                value="{{ $product->product_price }}">

                                                            <ins>{{ number_format($product->product_price) }}<sup
                                                                    style="font-size: 12px">vnđ</sup></ins>
                                                        </span>
                                                        <a class="tg-btn tg-btnstyletwo add_to_cart"
                                                            href="javascript:void(0);"
                                                            data-url="{{ route('addToCart', ['id' => $product->id]) }}"
                                                            style="width: 250px">
                                                            <i class="fa fa-shopping-basket"></i>
                                                            <em>Thêm vào giỏ hàng</em>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="row">
                                        <div class="col-12" style="margin-left: 15px">
                                            {{ $productCategories->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--************************************
                Author Detail End
        *************************************-->
        @include('cart.add-to-cart')
    </main>
@endsection
