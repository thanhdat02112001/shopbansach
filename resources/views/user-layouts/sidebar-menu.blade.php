<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 pull-left">
    <aside id="tg-sidebar" class="tg-sidebar">
        <div class="tg-widget tg-catagories">
            <div class="tg-widgettitle">
                <h3>Tất cả thể loại</h3>
            </div>
            <div class="tg-widgetcontent">
                <ul>
                    @foreach ($categories as $category)
                        <li><a
                                href="/category_detail/{{ $category->id }}"><span>{{ $category->category_name }}</span><em></em></a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </aside>
</div>
