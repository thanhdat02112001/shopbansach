<div class="tg-navigationarea">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <nav id="tg-nav" class="tg-nav">
                    <div class="navbar-header">

                    </div>
                    <div id="tg-navigation" class="collapse navbar-collapse tg-navigation">
                        <ul>
                            <li class="menu-item-has-children current-menu-item">
                                <a href="{{ route('home') }}">Trang chủ</a>
                                <ul class="sub-menu">
                                    <li><a href="{{ route('books') }}">Sách</a></li>
                                    <li><a href="{{ 'authors-page' }}">Tác giả</a></li>

                                </ul>
                            </li>
                            <li class="menu-item-has-children menu-item-has-mega-menu">
                                <a href="javascript:void(0);">Tất cả danh mục</a>
                                <div class="mega-menu">
                                    <ul class="tg-themetabnav" role="tablist">
                                        @foreach ($categories as $category)
                                            <li role="presentation" class="active">
                                                <a href="#artandphotography" aria-controls="artandphotography"
                                                    role="tab" data-toggle="tab">{{ $category->category_name }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                            <li class="menu-item">
                                <a href="{{ 'authors-page' }}">Tác giả</a>
                            </li>
                            <li><a href="products.html">Bán chạy nhất</a></li>
                            <li><a href="products.html">Giảm giá hàng tuần</a></li>
                            <li class="menu-item-has-children">
                                <a href="javascript:void(0);">Tin mới nhất</a>
                                <ul class="sub-menu">
                                    <li><a href="newslist.html">Danh sách tin</a></li>

                                    <li><a href="newsdetail.html">Chi tiết tin</a></li>
                                </ul>
                            </li>
                            <li><a href="contactus.html">Liên hệ</a></li>
                            <li class="menu-item-has-children current-menu-item">
                                <a href="javascript:void(0);"><i class="icon-menu"></i></a>
                                <ul class="sub-menu">
                                    <li class="menu-item-has-children">
                                        <a href="aboutus.html">Sản phẩm</a>
                                        <ul class="sub-menu">
                                            <li><a href="products.html">Sản phẩm</a></li>
                                            <li><a href="productdetail.html">Chi tiết sản phẩm</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="aboutus.html">Giới thiệu</a></li>

                                    <li><a href="/comingsoon">Sắp ra mắt</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>
