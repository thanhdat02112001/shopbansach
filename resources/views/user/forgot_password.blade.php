@extends('layouts.without-user')
@section('user')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align: center">
                        <h3 class="panel-title">Quên mật khẩu</h3>
                    </div>
                    <div class="panel-body">
                        <form accept-charset="UTF-8" role="form">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Email" name="email" type="text"
                                        style="border: 1px solid black">
                                </div>
                                <input class="btn btn-lg btn-success btn-block" type="submit" value="Lấy lại mật khẩu">
                            </fieldset>
                        </form>

                        <a href="/login">Quay về trang đăng nhập</a>
                    </div>
                </div>
            </div>
        </div>
</div @endsection
