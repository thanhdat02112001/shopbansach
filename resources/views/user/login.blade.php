@extends('layouts.user-page-main')
@section('user')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div style="margin-top:0px; text-align:center; display:flex; justify-content:center">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (session('notice'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('notice') }}
                        </div>
                    @endif
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align: center">
                        <h3 class="panel-title">Đăng nhập</h3>
                    </div>
                    <div class="panel-body">
                        <form accept-charset="UTF-8" role="form" method="POST" action="/login">
                            @csrf
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Email" name="email" type="text"
                                        style="border: 1px solid black">
                                    @error('email')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Mật khẩu" name="password" type="password"
                                        value="" style="border: 1px solid black">
                                    @error('password')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>

                                <label>
                                    <input name="remember" type="checkbox" value="Remember Me"> Nhớ mật khẩu
                                </label>

                                <input class="btn btn-lg btn-success btn-block" type="submit" value="Đăng nhập">
                            </fieldset>
                        </form>
                        <a href="/forgot-password">Quên mật khẩu?</a>
                        <p>Chưa có tài khoản? <a href="/resigter">Đăng kí</a></p>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
