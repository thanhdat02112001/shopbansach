@extends('layouts.user-page-main')
@section('user')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">

                    <div class="panel-heading" style="text-align: center">
                        <h3 class="panel-title">Đăng ký</h3>
                    </div>
                    <div class="panel-body">
                        <form accept-charset="UTF-8" role="form" method="POST" action="/resigter">
                            @csrf

                            <div class="form-group">
                                <input class="form-control" placeholder="Tên người dùng" name="name" type="text"
                                    style="border: 1px solid black">
                                @error('name')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Email" name="email" type="text"
                                    style="border: 1px solid black">
                                @error('email')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Địa chỉ" name="address" type="text"
                                    style="border: 1px solid black">
                                @error('email')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Mật khẩu" name="password" type="password"
                                    value="" style="border: 1px solid black">
                                @error('password')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>


                            <div class="form-group">
                                <input class="form-control" placeholder="Số điện thoại" name="phone_number" type="text"
                                    style="border: 1px solid black">
                                @error('phone_number')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>

                            <label>
                                <input name="Agree" type="checkbox" value="Remember Me"> Tôi đồng ý với điều khoản và điều
                                kiện
                            </label>

                            <input class="btn btn-lg btn-success btn-block" type="submit" value="Đăng ký">

                        </form>
                        <p>Đã có tài khoản? <a href="/login">Đăng nhập ngay</a></p>
                    </div>
                </div>
            </div>
        </div>
</div @endsection
