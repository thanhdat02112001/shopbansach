@extends('layouts.main')
@section('user')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align: center">
                        <h3 class="panel-title">Đổi mật khẩu</h3>
                    </div>
                    <div class="panel-body">
                        <form accept-charset="UTF-8" role="form">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Mật khẩu cũ" name="old_password" type="text"
                                        style="border: 1px solid black">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Mật khẩu mới" name="new_password"
                                        type="password" value="" style="border: 1px solid black">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Nhập lại mật khẩu mới" name="cf_new_password"
                                        type="password" value="" style="border: 1px solid black">
                                </div>

                                <input class="btn btn-lg btn-success btn-block" type="submit" value="Đổi mật khẩu">
                            </fieldset>
                        </form>
                        <a href="#">Về trang chủ</a>

                    </div>
                </div>
            </div>
        </div>
</div @endsection
