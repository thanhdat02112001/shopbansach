 <!-- Sidebar Menu -->
 <nav class="mt-2">
     <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
         <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
         <li class="nav-item">
             <a href="{{ route('dashboard-v1') }}" class="nav-link">
                 <i class="nav-icon fas fa-tachometer-alt"></i>
                 <p>
                     Bảng thống kê
                     <i class="right fas fa-angle-left"></i>
                 </p>
             </a>
             <ul class="nav nav-treeview">
                 <li class="nav-item">
                     <a href="{{ route('dashboard-v1') }}" class="nav-link">
                         <i class="far fa-circle nav-icon"></i>
                         <p>Doanh thu bán hàng</p>
                     </a>
                 </li>
                 <li class="nav-item">
                     <a href="{{ route('dashboard-v2') }}" class="nav-link">
                         <i class="far fa-circle nav-icon"></i>
                         <p>Thống kê đơn hàng</p>
                     </a>
                 </li>
             </ul>
         </li>

         <li class="nav-item">
             <a href="#" class="nav-link">
                 <i class="nav-icon fas fa-book"></i>
                 <p>
                     Danh sách của sách
                     <i class="fas fa-angle-left right"></i>
                 </p>
             </a>
             <ul class="nav nav-treeview">
                 <li class="nav-item">
                     <a href="{{ route('list-product') }}" class="nav-link">
                         <i class="far fa-circle nav-icon"></i>
                         <p>Xem danh sách của sách</p>
                     </a>
                 </li>
                 <li class="nav-item">
                     <a href="{{ route('create-product') }}" class="nav-link">
                         <i class="far fa-circle nav-icon"></i>
                         <p>Thêm sách mới</p>
                     </a>
                 </li>
             </ul>
         </li>
         <li class="nav-item">
             <a href="#" class="nav-link">
                 <i class="nav-icon fas fa-table"></i>
                 <p>
                     Danh sách thể loại
                     <i class="fas fa-angle-left right"></i>
                 </p>
             </a>
             <ul class="nav nav-treeview">
                 <li class="nav-item">
                     <a href="{{ route('list-category') }}" class="nav-link">
                         <i class="far fa-circle nav-icon"></i>
                         <p>Xem danh sách thể loại</p>
                     </a>
                 </li>
                 <li class="nav-item">
                     <a href="{{ route('create-category') }}" class="nav-link">
                         <i class="far fa-circle nav-icon"></i>
                         <p>Thêm thể loại mới</p>
                     </a>
                 </li>
             </ul>
         </li>
         <li class="nav-item">
             <a href="#" class="nav-link">
                 <i class="nav-icon fas fa-user-edit"></i>
                 <p>
                     Danh sách tác giả
                     <i class="fas fa-angle-left right"></i>
                 </p>
             </a>
             <ul class="nav nav-treeview">
                 <li class="nav-item">
                     <a href="{{ route('list-author') }}" class="nav-link">
                         <i class="far fa-circle nav-icon"></i>
                         <p>Xem danh sách tác giả</p>
                     </a>
                 </li>
                 <li class="nav-item">
                     <a href="{{ route('create-author') }}" class="nav-link">
                         <i class="far fa-circle nav-icon"></i>
                         <p>Thêm tác giả mới</p>
                     </a>
                 </li>
             </ul>
         </li>
         <li class="nav-item">
             <a href="#" class="nav-link">
                 <i class="nav-icon fas fa-house-user"></i>
                 <p>
                     Danh sách nhà xuất bản
                     <i class="fas fa-angle-left right"></i>
                 </p>
             </a>
             <ul class="nav nav-treeview">
                 <li class="nav-item">
                     <a href="{{ route('list-supplier') }}" class="nav-link">
                         <i class="far fa-circle nav-icon"></i>
                         <p>Xem nhà xuất bản</p>
                     </a>
                 </li>
                 <li class="nav-item">
                     <a href="{{ route('create-supplier') }}" class="nav-link">
                         <i class="far fa-circle nav-icon"></i>
                         <p>Thêm nhà xuất bản mới</p>
                     </a>
                 </li>
             </ul>
         </li>
         <li class="nav-item">
             <a href="#" class="nav-link">
                 <i class="nav-icon fas fa-cart-arrow-down"></i>
                 <p>
                     Trang bán hàng
                     <i class="fas fa-angle-left right"></i>
                 </p>
             </a>
             <ul class="nav nav-treeview">
                 <li class="nav-item">
                     <a href="{{ route('pages-invoice') }}" class="nav-link">
                         <i class="far fa-circle nav-icon"></i>
                         <p>Đơn hàng mới</p>
                     </a>
                 </li>
                 <li class="nav-item">
                     <a href="{{ route('page-projects') }}" class="nav-link">
                         <i class="far fa-circle nav-icon"></i>
                         <p>Tình trạng đơn hàng</p>
                     </a>
                 </li>
             </ul>
         </li>
     </ul>
 </nav>
 <!-- /.sidebar-menu -->
