<div class="cart" data-url="{{ route('deleteCart') }}">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <table class="table table-hover update_cart_url" data-url="{{ route('updateCart') }}">
                    <thead>
                        <tr>
                            <th>Sản phẩm</th>
                            <th>Số lượng</th>
                            <th class="text-center">Giá</th>
                            <th class="text-center">Tạm tính</th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $total = 0; ?>
                        @foreach ($carts as $id => $cart)
                            <tr>
                                <td class="col-sm-8 col-md-4">
                                    <div class="media">
                                        <a class="thumbnail pull-left" href="#"> <img class="media-object"
                                                src="{{ Storage::disk('product_avatar')->url($cart['image']) }}"
                                                style="width: 72px; height: 72px;"> </a>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a href="#">{{ $cart['name'] }}</a></h4>
                                        </div>
                                    </div>
                                </td>
                                <td class="col-sm-1 col-md-2">
                                    <button class="minus btn"><i class="fas fa-minus"></i></button>
                                    <input type="number" value="{{ $cart['quantity'] }}" id="quantity" min="1"
                                        style="width:40px; height:40px; padding: 0px 5px" data-id="{{ $id }}">
                                    <button class="plus btn"><i class="fas fa-plus"></i></button>
                                </td>
                                <td class="col-sm-1 col-md-1 text-center">
                                    <strong>{{ number_format($cart['price']) }}</strong> <strong>VND</strong></td>
                                <td class="col-sm-1 col-md-1 text-center">
                                    <strong>{{ number_format($cart['price'] * $cart['quantity']) }}</strong>
                                    <strong>VND</strong></td>
                                <td class="col-sm-1 col-md-1">
                                    <button type="button" class="btn btn-success cart_update" id='update'
                                        data-id="{{ $id }}"><i class="fas fa-save"></i></button>
                                    <button type="button" class="btn btn-danger cart_delete"
                                        data-id="{{ $id }}">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>

                                </td>
                            </tr>
                            <?php $total += $cart['price'] * $cart['quantity']; ?>
                        @endforeach
                        <tr>
                            <td>   </td>
                            <td>   </td>
                            <td>   </td>
                            <td>
                                <h3>Thành tiền</h3>
                            </td>
                            <td class="text-right">
                                <h3><strong>{{ number_format($total) }}</strong> <strong>VND</strong></h3>
                            </td>
                        </tr>
                        <tr>
                            <td>   </td>
                            <td>   </td>
                            <td>   </td>
                            <td>
                                <a href="{{ route('home') }}" class="btn btn-default"><i
                                        class="glyphicon glyphicon-shopping-cart"></i>Tiếp tục mua sắm</a>
                            </td>
                            <td>
                                <a href="/checkout"><button class="btn btn-success" id="checkout"><i
                                            class="glyphicon glyphicon-play"></i> Thanh toán</button></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"
integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
        let quantity = $("#quantity").val();
        if (quantity < 0) {
            alert('số lượng không hợp lệ');
            $('#checkout').prop('disabled', true);
        }
    });
</script>
