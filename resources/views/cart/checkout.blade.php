@extends('layouts.user-page-main')
@section('checkout')

    <body>

        <main role="main">
            <!-- Block content - Đục lỗ trên giao diện bố cục chung, đặt tên là `content` -->
            <div class="container mt-4">
                <form class="needs-validation" name="frmthanhtoan" method="post" action="{{ route('postPay') }}">
                    @csrf


                    <div class="py-5 text-center">
                        <i class="fa fa-credit-card fa-4x" aria-hidden="true"></i>
                        <h2>Thanh toán</h2>
                        <p class="lead">Vui lòng kiểm tra thông tin Khách hàng, thông tin Giỏ hàng trước khi Đặt
                            hàng.</p>
                    </div>
                    <div class="row">
                        <div class="col-md-4 order-md-2 mb-4">
                            <h4 class="d-flex justify-content-between align-items-center mb-3">
                                <span class="text-muted">Giỏ hàng</span>
                                <span
                                    class="badge badge-secondary badge-pill">{{ session()->get('cart') !== null ? count(session()->get('cart')) : 0 }}</span>
                            </h4>
                            <ul class="list-group mb-3">
                                <?php $total = 0; ?>
                                @foreach ($carts as $id => $cart)
                                    <li class="list-group-item d-flex justify-content-between lh-condensed">

                                        <a class="thumbnail pull-left" href="#"> <img class="media-object"
                                                src="{{ Storage::disk('product_avatar')->url($cart['image']) }}"
                                                style="width: 72px; height: 72px;"> </a>

                                        <div style="margin-left: 200px">
                                            <input type="hidden" name="product_name[]" id="" value="{{ $cart['name'] }}">
                                            <input type="hidden" name="product_price[]"
                                                value="{{ number_format($cart['price']) }}">
                                            <input type="hidden" name="product_quantity[]" value="{{ $cart['quantity'] }}">
                                            <h6 class="my-0">{{ $cart['name'] }}</h6>
                                            <small class="text-muted">{{ number_format($cart['price']) }} x
                                                {{ $cart['quantity'] }}</small> <br>
                                            <span
                                                class="text-muted">{{ number_format($cart['price'] * $cart['quantity']) }}
                                                VND</span>
                                            <input type="hidden" value=" {{ $total += $cart['price'] * $cart['quantity'] }}">
                                        </div>

                                    </li>
                                @endforeach
                                <div>
                                    <p><strong>Tổng tiền: {{ number_format($total) }} VND</strong> </p>
                                    <input type="hidden" name="total" value="{{ $total }}">
                                </div>
                            </ul>


                            {{-- <div class="input-group">
                            <input type="text" class="form-control" placeholder="Mã khuyến mãi">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-secondary">Xác nhận</button>
                            </div>
                        </div> --}}

                        </div>
                        <div class="col-md-8 order-md-1">
                            <h4 class="mb-3">Thông tin khách hàng</h4>

                            <div class="row">
                                <div class="col-md-12">
                                    <label for="kh_ten">Họ tên</label>
                                    <input type="text" class="form-control" name="kh_ten" id="kh_ten" value="">
                                </div>
                                <div class="col-md-12">
                                    <label for="kh_diachi">Địa chỉ</label>
                                    <input type="text" class="form-control" name="kh_diachi" id="kh_diachi" value="">
                                </div>
                                <div class="col-md-12">
                                    <label for="kh_dienthoai">Điện thoại</label>
                                    <input type="text" class="form-control" name="kh_dienthoai" id="kh_dienthoai"
                                        value="">
                                </div>
                                <div class="col-md-12">
                                    <label for="kh_email">Email</label>
                                    <input type="text" class="form-control" name="kh_email" id="kh_email" value="">
                                </div>
                                <div class="col-md-12">
                                    <label for="kh_note">Ghi chú</label>
                                    <input type="text" class="form-control" name="kh_note" id="kh_note" value="">
                                </div>
                            </div>

                            <h4 class="mb-3">Hình thức thanh toán</h4>

                            <div class="row">
                                <div class="col-md-3">

                                    <label>
                                        <input type="radio" name="httt_ma" value="1" class="custom-control-input">
                                        Tiền Mặt
                                    </label>

                                </div>
                                <div class="col-md-3">
                                    <label>
                                        <input type="radio" name="httt_ma" class="custom-control-input" value="2">
                                        VN Pay
                                    </label>

                                </div>

                            </div>
                            <hr class="mb-4">
                            <button class="btn btn-primary btn-lg btn-block" type="submit" name="btnDatHang">Đặt
                                hàng</button>
                        </div>
                    </div>
                </form>

            </div>
            <!-- End block content -->
        </main>
    @endsection
