<script src="https://code.jquery.com/jquery-3.6.0.min.js"
integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
    $(document).on('click', '.minus', function() {
        let input = $(this).next();
        let quantity = parseInt(input.val()) - 1;
        if (quantity < 1) {
            quantity = 1;
        }
        input.val(quantity);

    })
    $(document).on('click', '.plus', function() {
        let input = $(this).prev();
        let quantity = parseInt(input.val()) + 1;

        input.val(quantity);

    })
</script>
