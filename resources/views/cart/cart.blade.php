@extends('layouts.user-page-main')
@section('cart')
    <div class="cart_wrapper">
        @include('cart.components.cart_component')
    </div>
    @include('cart.update-cart')

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script>
        function cartUpdate(e) {
            e.preventDefault();
            let urlUpdateCart = $(".update_cart_url").data('url');
            let id = $(this).data('id');
            let quantity = $(this).parents('tr').find('input').val();
            $.ajax({
                method: 'GET',
                url: urlUpdateCart,
                data: {
                    id: id,
                    quantity: quantity
                },
                success: function(data) {
                    if (data.status === 200) {
                        $('.cart_wrapper').html(data.cart_component);
                    }
                },
                error: function() {
                }
            })
        }

        function cartDelete(e) {
            e.preventDefault();
            let urlDelete = $('.cart').data('url');
            let id = $(this).data('id');
            $.ajax({
                method: 'GET',
                url: urlDelete,
                data: {
                    id: id
                },
                success: function(data) {
                    if (data.status === 200) {
                        $('.cart_wrapper').html(data.cart_component);
                    }
                },
                error: function() {

                }
            })
        }

        function editAll(e) {
            e.preventDefault();
            let list = [];
            $('table tbody tr td').each(function() {
                $(this).find('input').each(function() {
                    let element = {
                        key: $(this).data('id'),
                        value: $(this).val()
                    };
                    list.push(element);

                });
            });
            $.ajax({
                method: "POST",
                url: '/save-all',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "data": list
                },
                success: function() {
                    if (data.status === 200) {
                        $('.cart_wrapper').html(data.cart_component);
                    }
                },
                error: function() {

                }
            })
        }
        $(function() {
            $(document).on('click', '.cart_update', cartUpdate);
            $(document).on('click', '.cart_delete', cartDelete);
            $(document).on('click', '.edit-all', editAll);

        })
    </script>
@endsection
