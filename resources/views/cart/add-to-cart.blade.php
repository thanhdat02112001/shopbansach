<script src="https://code.jquery.com/jquery-3.6.0.min.js"
integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
    function addToCart(event) {
        event.preventDefault();
        let url = $(this).data('url');
        $.ajax({
            method: "GET",
            url: url,
            dataType: 'json',
            success: function(data) {
                if (data.code === 200) {
                    alert('Thêm sản phẩm thành công');
                }
            },
            error: function() {

            }

        })
    }
    $(function() {
        $('.add_to_cart').on('click', addToCart)
    })
</script>
