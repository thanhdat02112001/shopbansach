<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplieresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplieres', function (Blueprint $table) {
            $table->id();
            $table->string('supplier_name', 50);
            $table->string('supplier_phone', 50);
            $table->string('supplier_email', 50)->unique();
            $table->string('supplier_address', 255);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplieres');
        Schema::table('supplieres', function (Blueprint $table) 
        {
            $table->dropSoftDeletes();
        });
    }
}
