 <?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id()->unsigned();
            $table->String('product_name');
            $table->String("product_slug")->unique();
            $table->double("product_price");
            $table->integer('product_sale')->default(0);
            $table->integer('product_quantity');
            $table->String("product_avatar");
            $table->text("product_description");
            $table->text("product_content");
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
        Schema::table('products', function (Blueprint $table) 
        {
            $table->dropSoftDeletes();
        });
    }
}
