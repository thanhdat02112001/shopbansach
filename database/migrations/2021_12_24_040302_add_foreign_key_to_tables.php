<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeyToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->foreignId("product_category_id")->references("id")->on("categories");
            $table->foreignId("product_supplier_id")->references("id")->on("supplieres");
            $table->foreignId("product_author_id")->references("id")->on("authors");
        });
        Schema::table("avatars",function(Blueprint $table){
            $table->foreignId("avatar_product_id")->references("id")->on("products");
        });

        Schema::table("comments",function(Blueprint $table){
            $table->foreignId("comment_admin_id")->references("id")->on("admins");
            $table->foreignId("comment_user_id")->references("id")->on("users");
            $table->foreignId("comment_product_id")->references("id")->on("products");

        });
        Schema::table("orders",function(Blueprint $table){
            $table->foreignId("order_transaction_id")->references("id")->on("transactions");
        });

        Schema::table("rates", function(Blueprint $table){
            $table->foreignId("rate_user_id")->references("id")->on("users");
            $table->foreignId("rate_product_id")->references("id")->on("products");
        });

        Schema::table("transactions", function(Blueprint $table){
            $table->foreignId("transaction_user_id")->references("id")->on("users");
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products',function(Blueprint $table)     {
            $table->dropForeign('categories_product_category_id_foreign');
            $table->dropColumn('product_category_id');
            $table->dropForeign("supplieres_product_supplier_id_foreign");
            $table->dropColumn("product_supplier_id");
            $table->dropForeign("authors_product_author_id_foreign");
            $table->dropColumn("product_author_id");
        });
        Schema::table("avatars",function(Blueprint $table){
            $table->dropForeign("products_avatar_product_id_foreign");
            $table->dropColumn("avatar_product_id");
        });

        Schema::table("comments",function(Blueprint $table){
            $table->dropForeign("admins_comment_admin_id_foreign");
            $table->dropColumn("comment_admin_id");
            $table->dropForeign("users_comment_user_id_foreign");
            $table->dropColumn("comment_user_id");
            $table->dropForeign("products_comment_product_id_foreign");
            $table->dropColumn("comment_product_id");
        });
        Schema::table("orders",function(Blueprint $table){
            $table->dropForeign("transactions_order_transaction_id_foreign");
            $table->dropColumn("order_transaction_id");
        });
        Schema::table("rates", function(Blueprint $table){
            $table->dropForeign("users_rate_user_id_foreign");
            $table->dropColumn("rate_user_id");
            $table->dropForeign("products_rate_product_id_foreign");
            $table->dropColumn("rate_product_id");
        });

        Schema::table("transactions", function(Blueprint $table){;
            $table->dropForeign("users_transaction_user_id_foreign");
            $table->dropColumn("transaction_user_id");
        });
    }
}
